# README #
This is my hexo blog's generator and deployer using hexo.  
It is not complete.  
prerequisite: `git`, `node`, `hexo`  
One should run `npm install` under blog folder after `git clone` this blog.  
`package.json` is required, but it depends on platforms.  
`_config.yml` is required.  

How to build up this blog,  

```bash
# brew install git
# brew install npm
npm install hexo -g

# under this git repo folder
npm install
hexo s -g

# deployment according to yml
# do this if you don't want to lose your commits
# cd .deploy_git/your_site_name && git clone repo_of_blog_at_deploy_branch
# deploy to remote repo
hexo deploy
```

this is only needed when you just are installing your hexo project on a new computer and running the very first deployment on that computer. 

if you don't need to access via your own domain, remove CNAME.  
