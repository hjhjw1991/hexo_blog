---
layout: post
title: 【Android源码伴读】ListView那些事儿
category: android
description: 讲述Android中ListView的实现原理和自己遇到的问题 --施工中
tags: android
date: 2015-10-20
---

>事情的起因，是ListView遇到了一个bug：嵌套ScrollView的时候只显示一个Item。

## ScrollView嵌套ListView时遇到Item显示不正确，上下滑动内存泄漏等问题
### 1 不使用ScrollView
### 2 在添加Item后，重新手工进行measure设置ListView的高度
## ListView分析
### 类结构
### 设计模式
### 生命周期
## 自己动手写不同效果的ListView
### 滑动删除
### pin
### 拼音首字母导航
### 搜索且即时显示结果
