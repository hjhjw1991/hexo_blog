---
layout: post
title: 【Leetcode题解】65-Valid Number
category: leetcode
description: 合法数 Level:Hard
date: 2014-12-26
---

>本系列代码见[我的Leetcode仓库](https://github.com/hjhjw1991/leetcode)

## [题目][Leetcode]

Validate if a given string is numeric.

Some examples:

`"0"` => `true`

`" 0.1 "` => `true`

`"abc"` => `false`

`"1 a"` => `false`

`"2e10"` => `true`

__Note__: It is intended for the problem statement to be ambiguous. You should gather all requirements up front before implementing one.

这个题被标注为Hard，但我没看出来哪里Hard，估计是因为AC率只有11%？

## 思路
想case，各种case，当然首先是合法的，然后是非法的，想完构造正则表达式，返回是否匹配，没了。

* 合法case:
	* " 1"
	* "1.01"
	* "1.0"
	* **"1."**
	* **".1"**
	* "1e204"
	* **"+.3"**
	* "-002e+3.4"
* 非法case:
	* "各种包含e以外字母的串"
	* "-2e"
	* "1. 1"
	* " "
	* ""
	* 其实这一部分都是扯淡，只要合法case全都考虑到，其余的都是非法case
	* 经测试，题目的testcase没有考虑形如log这样的表达式
	
另外，如果不允许使用Java自带的正则匹配，那么需要自己实现一个自动机，只要正确构造出题目期待的状态机就可以解决。

## 代码

**Language:java**
<!-- lang:java -->

	import java.util.regex.Matcher;
	import java.util.regex.Pattern;
	public class Solution {
    	public boolean isNumber(String s) {
        	String regex =  "^(\\s*)([\\-+])?((\\d+(\\.\\d*)?)|(\\.\\d+))(e([\\-+])?\\d+)?(\\s*)$";
    		Pattern p = Pattern.compile(regex);
    		Matcher m=p.matcher(s);
    		return m.matches();
    	}
	}

[Leetcode]: https://oj.leetcode.com/problems/valid-number/