---
layout: post
title: 使用adb工具调试android程序(一)
description: 作为adb调试android程序的第一步，需要学习如何用adb工具连接设备，安装卸载程序和getshell
category: blog
tags: [android]
date: 2016-2-22
---

>尽管现在完善的IDE提供给我们一些android debugger工具，但有时打开IDE和使用这些工具可能无法满足我们的某些要求，或者手边暂时没有趁手的IDE可用，这时候使用adb直接进行开发和调试就成了权宜之计。另外如果我们清楚android sdk提供给我们的常用工具工作原理，那么就可以自行编写功能脚本完成一些较为复杂的调试流程，例如trace或者dump之后自动标记重点区域，或者连接外部dex进行编译等。  

工欲善其事，必先利其器。以前在Eclipse的时候就觉得DDMS是应用开发调试的神器，现在的Android Studio也集成了Android Debugger，同样强大。而他们背后，都是android sdk所带的调试工具，adb。 

## 基本功能 
adb启动的时候会默认监听两个端口，5553是daemon端口，5554是设备端口。  
借助adb，我们可以管理设备或手机模拟器的状态。还可以进行以下的操作:  

1. 快速更新设备或手机模拟器中的代码，如应用或Android 系统升级  
2. 在设备上运行shell命令  
3. 管理设备或手机模拟器上的预定端口  
4. 在设备或手机模拟器上复制或粘贴文件  

## 一个简单的例子
我们想要将编译得来的或者下载得来的apk安装到目标设备上  
```
adb devices # 获取在线设备
adb install -r xxx.apk # 安装apk，有多个选项，-r表示replace已有的app
```
我们想上传一些文件  
```
adb push local remote
```
我们想查看日志  
```
adb logcat [ <filter-spec> ]
```
我们想进入设备的shell  
```
adb shell
```

## 常用命令
1. 显示系统中全部Android平台  
```bash
android list targets
```
2. 显示系统中全部AVD（模拟器）  
```
android list avd
```
3. 创建AVD（模拟器）  
```
android create avd --name 名称 --target 平台编号
```
4. 启动模拟器  
```
emulator -avd 名称 -sdcard ~/名称.img (-skin 1280x800)
```
5. 删除AVD（模拟器）  
```
android delete avd --name 名称
```
6. 创建SDCard  
```
mksdcard 1024M ~/名称.img
```
7. AVD(模拟器)所在位置  
```
Linux(~/.android/avd)
Windows(C:\Documents and Settings\Administrator\.android\avd)
```
8. 启动DDMS  
```
ddms
```
9. 显示当前运行的全部模拟器  
```
adb devices
```
10. 对某一模拟器执行命令  
```
adb -s 模拟器编号 命令
```
11. 安装应用程序  
```
adb install -r 应用程序.apk
```
12. 获取模拟器中的文件  
```
adb pull <remote> <local>
```
13. 向模拟器中写文件  
```
adb push <local> <remote>
```
14. 进入模拟器的shell模式  
```
adb shell
```
15. 启动SDK，文档，实例下载管理器  
```
android
```
16. 卸载apk包  
```
adb shell
cd data/app
rm apk包
exit
adb uninstall apk包的主包名
adb install -r apk包
```
17. 查看adb命令帮助信息  
```
adb help
```
18. 在命令行中查看LOG信息  
```
adb logcat -s 标签名
```
19. adb shell后面跟的命令主要来自`源码\system\core\toolbox目录和源码\frameworks\base\cmds`目录。

20. 删除系统应用  
```
adb remount （重新挂载系统分区，使系统分区重新可写）。
adb shell
cd system/app
rm *.apk
```
21. 获取管理员权限  
```
adb root
```
22. 启动Activity  
```
adb shell am start -n 包名/包名＋类名（-n 类名,-a action,-d date,-m MIME-TYPE,-c category,-e 扩展数据,等）
```
23. 发布端口  
```
# 你可以设置任意的端口号，做为主机向模拟器或设备的请求端口。如  
adb forward tcp:5555 tcp:8000
```
24. 复制文件  
```
# 你可向一个设备或从一个设备中复制文件  

# 复制一个文件或目录到设备或模拟器上  
adb push <source> <destination></destination></source> 
# 如：adb push test.txt /tmp/test.txt 

# 从设备或模拟器上复制一个文件或目录  
adb pull <source> <destination></destination></source> 
# 如：adb pull /addroid/lib/libwebcore.so .
```
25. 搜索模拟器/设备的实例  
```
# 取得当前运行的模拟器/设备的实例的列表及每个实例的状态： 
adb devices
```
26. 查看bug报告  
```
adb bugreport 
```
27. 记录无线通讯日志  
```
# 一般来说，无线通讯的日志非常多，在运行时没必要去记录，但我们还是可以通过命令，设置记录  
adb shell  
logcat -b radio
```
28、获取设备的ID和序列号  
```
adb get-product 
adb get-serialno
```
29、访问数据库SQLite3  
```
adb shell 
sqlite3
```