---
layout: post
title: Windows下使用Putty配置github-ssh-key
description: 半夜重新配置了一下Github的sshkey
category: blog
tags: [git,ssh,windows]
date: 2015-08-16
---

以前的sshkey配置好之后从来没有使用过，发现可能是配置有问题，所以全部删除了重新配置。添加sshkey的时候，遇到问题说key is invalid。知道是sshkey格式问题，Putty保存的public key格式跟github认得的格式不同。  
查阅互联网，结合问题信息，在public key开头添加了`ssh-rsa `并回车，在结尾新加一行写了邮件地址`hjhjw1991@gmail.com `，去掉putty生成的comment，再复制保存到公钥输入框，搞定。  
接下来要验证sshkey，在sourcetree启动git bash，按照github官方说明打测试`ssh -T git@github.com`，结果提示permission denied。显然是ssh key不认识，根据官方文档添加verbose信息，`ssh -vT git@github.com`后看到，bash他在我用户目录下的.ssh文件夹下搜索名为id_rsa id_dsa等三个固定名字的私钥。OK，问题应该在这里了。我把openssh格式的私钥（之前在ubuntu上用openssh生成的，putty使用的ppk是不能用的，导出成openssh格式之后也不一定能用）放在了.ssh目录下，再测试连接，成功。  
然后发现我从来没用过ssh方式连接github，都是用的https 验证用户名密码的方式，喜大普奔。  

---
8-28  
putty生成的密钥正文是被换行符隔断了的，得删掉  

---
11-09  
其实ssh2的正确格式是每个字段以单个空格为分隔符，github只是对此做了优化，把多个空格、回车和tab也作为分隔符了。  

参考资料：[Linux SSH-RSA 公约格式](http://www.myext.cn/other/a_25835.html)  
以下为要点:  
>
SSH 1 公钥格式  
>>
	$ cat id-rsa1.pub  
	2048 65537 22910674242901951372109700514222305911005404382729826828303733202727246271129151780809328111314142444937643708197065336344061806573173759249496873454280552699704169728818376102327988617178797568649601671735522868137145364100426211002912160142668644115040507860106304962512271803001264027359406587863651664228014572232130610186228786600276455187805301282374669236217832825274065088078401281168028793885110785700945596016775112537678694431532570328169049252968039090407234623028954479983169424889698023716686474015246450335458030057416596685148084608420001646747215709382718194528577791195697244767428069870538621149641 xavier@Qbee-X
 	
>>
　　SSH 1 公钥格式为所有字段以单个空格符分隔，各字段依次为选项、位数、指数、系数、注释。第一个字段是可选的，表示该条目（行）是否以数字开头，选项字段不会以数字开头。最后一个字段注释，如果在生成密钥时没有给定注释，默认注释为密钥的创建者（一般就是 username@hostname 这种格式），注释仅仅是提供给用户查看密钥时作为一个辨识标记，在 SSH 使用中没有任何作用。  
>
SSH 2 公钥格式  
>>
	$ cat id-rsa.pub  
	ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDmg/9Hoc98c6AIlEPYK6VqE3ZTlMlfXqn2VgU0NAPXeRmYX+jHYZXA5AxMNTD8KgZIuilK23qzzKBy0hgoO9DDpsBg98Pf4NLkHjTcjpPFX1TBD6RWO/OX/g4uN+gZH1/zoREJa4fit8SSG2V2GoQkqrhfRYCy+a9mQX29ZvOLfYK/aMVjb+yfzLJychdvV3CQkDvkd5R/DHNU6ZQ3pyIEjA/cUQqqmgxQj0f72y8IXv8zesXtjXaaKG17R2oSN1MNDZo/y4m426MYxl4mMQMvM5yR3UvhD93pgNgaR8LY/vspcIeE6cJZsGl21oYtrDZ/GdmF5ulMGwjIDfTJRLvb xavier@Qbee-X
>>
　　SSH 2 公钥格式为所有字段以单个空格符分隔，各字段依次为选项、密钥类型（keytype）、base64编码后的密钥、注释。第一个字段是可选的，表示该条目（行）是否以数字开头，选项字段不会以数字开头。最后一个字段注释，如果在生成密钥时没有给定注释，默认注释为密钥的创建者（一般就是 username@hostname 这种格式），注释仅仅是提供给用户查看密钥时作为一个辨识标记，在 SSH 使用中没有任何作用。
>>
　　密钥类型（keytype）可能是 ecdsa-sha2-nistp256, ecdsa-sha2-nistp384, ecdsa-sha2-nistp521, ssh-ed25519, ssh-dss 或 ssh-rsa。
>
SSH 2 - IETF SECSH 公钥格式
>>
	---- BEGIN SSH2 PUBLIC KEY ----
	AAAAB3NzaC1yc2EAAAABIwAAAIEAp7drPA+ZGAZtKNqbVxYWU5
	lYPhS5HPZbyozA+KgxPhOLHI1BVoxQfsGae2OUWoy8bpzPib+U
	ukILHyuEyct9RWv+3OVgEzxuq4KO9y3sK1pdxycvsKwWgXPiis
	LVO3Zf9Dz7hR0wKzC3+IOMXfDpcEyHRVN4sNsCtPXa8BVbSys=
	---- END SSH2 PUBLIC KEY ----

>>
　　IETF 规定的 SSH 公钥格式被定义在 RFC4716 文件，该规范规定：
　　起始行和结束行必须分别是：
起始行：
>>
---- BEGIN SSH2 PUBLIC KEY ----

>>
结束行：
>>
---- END SSH2 PUBLIC KEY ----
>>
　　可以添加其他头部，例如注释 Comment: user@example.com。详情参见 RFC4716。  

[hjhjw1991]:    http://hjhjw1991.github.io  "hjhjw1991"
