---
layout: post
title: Windows 7下查看dns缓存和网络连接状态
description: ipconfig和netstat命令，做个备忘
category: tips
tags: [windows]
date: 2015-12-14
---

>所有命令通过help查看选项含义，这里不写

## dns缓存

查看dns缓存

	ipconfig /displaydns

清空dns缓存

	ipconfig /flushdns

## 网络状态

查看网络连接进程

	netstat /aop tcp


[hjhjw1991]:    http://hjhjw1991.github.io  "hjhjw1991"
