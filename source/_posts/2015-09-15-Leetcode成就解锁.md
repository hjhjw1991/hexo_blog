---
layout: post
title: Leetcode成就解锁
description: 时至前天终于把Leetcode中免费的题目刷完了，200+。解锁成就Leetcode穷B
category: opinion
date: 2015-09-15
---

刷完免费的题之后有种很明显的提高的感觉，尤其是对于backtracking、dp以及tree的题目已经培养出了一种结构上的直觉。唯一还苦手的就是数学题了，实在是因为在纯数学题的解题上自己并没有太系统的算法设计知识，就算是算法优化这方面也只是凭直觉和简单的分析在做。  
明白自己基础薄弱所以提高的感觉很明显，希望过一段时间二刷的时候能够有更大的进步。  
接下来着手刷竞赛级别的OJ  
有兴趣的话可以访问我的Leetcode解库，主要是用的Java，前期解题图方便用了Python，之后也会逐步补充上Java的解法。C++的解法是从别的我已经找不回原来主人的Git库里拿过来的，目的是与Java解法做对比。然而这个库比较老，只有150多道题。  
访问地址在这里[https://github.com/hjhjw1991/leetcode](https://github.com/hjhjw1991/leetcode)，当然欢迎star和fork，也欢迎提建议。 

[hjhjw1991]:    http://hjhjw1991.github.io  "hjhjw1991"
