---
layout: post
title: 【Android教程】View/ViewGroup源码阅读
category: android
description: 又叫【花样劝退教程】。本文讲View
tags: [android,guide,interview]
date: 2017-02-24
---

> 本文基于Android 4.0 源码

## View与ViewGroup类结构

![View与ViewGroup的类关系](/images/blogs/view_and_viewgroup.png)  

## View与Window
[Android应用setContentView与LayoutInflater加载解析机制源码分析]: http://blog.csdn.net/yanbober/article/details/45970721  

Android系统中所有的UI类都是建立在`View`和`ViewGroup`基础上。`View`的子类包括widget包中的那些(常用的`TextView`、`ImageView`等都在这个包下)，`ViewGroup`的子类包括Layout命名的类以及`RecyclerView`等少数以View命名的类，`ViewGroup`又是`View`的直接子类，这样就允许`View`和`ViewGroup`并列与嵌套存在。在AS中可以通过F4查看类的继承关系。  

Window不继承于View，它就是一个用来显示东西的窗口，通过`setContentView`来显示东西(调用该方法之后紧接着调用`initWindowDecorActionBar`来设置ActionBar)，`Activity`自身持有一个`mWindow`(初始化为`PhoneWindow`，`PhoneWindow`的定义在源码目录`frameworks/policies/base/phone/com/android/internal/policy/impl/PhoneWindow`，它是Window的直接子类，被标注为`@hide`，SDK看不到，需要下载SDK源码才能看到，在SDK源码的`android-23/com/android/internal/policy/PhoneWindow`下)，`Activity`的`setContentView`就是`mWindow.setContentView`的代理。`PhoneWindow`有个`DecorView`内部类(继承自`FrameLayout`)，在初始化`PhoneWindow`的时候会创建一个`DecorView`的实例作为根视图。  

还有一个类叫做`LayoutInflater`，它本身是个抽象类，但只有一个`cloneInContext`是抽象方法。它采用工厂模式负责将布局文件xml实例化为View对象。看它的`inflate`方法，最终调用的都是`View inflate(XmlPullParser parser, ViewGroup root, boolean attachToRoot)`这个方法，用`XmlResourceParser`从resource中获取parser，然后**同步地**完成这些调用：  
`result=root, attrs=Xml.asAttributeSet(parser)`  
-> `temp=createViewFromTag(root, name=parser.next().getName(), inflaterContext=mContext, attrs)`  
-> `root!=null?(params=root.generateLayoutParams(attrs);if(!attachToRoot)temp.setLayoutParams(params)):null`  
-> `rInflateChildren(parser, temp, attrs, true)`  
-> `if(root!=null && attachToRoot) root.addView(temp, params); if(root==null || !attachToRoot)result = temp`  
-> `return result`  
大致意思是首先解析xml，找到第一个开始的Tag即布局文件的根视图的标签，把它和布局文件的`AttributeSet`传给`createViewFromTag`就获得了布局文件的根View，如果root非空且要附加到root上就把这个View附加上去。在处理Tag时单独处理MERGE和INCLUDE两个Tag。在返回前调用了`rInflateChildren`方法来填充这个View的子视图，这是个递归填充子视图的方法，根据Tag名分别调用`parseRequestFocus parseViewTag parseInclude createViewFromTag&&rInflateChildren`来一层层创建View，在`finishInflate`的时候View提供了`onFinishInflate`回调供子类重写。  

这里就有三个东西了，Window、Inflater、View，他们之间的关系就好像画布、画笔和成品作品，xml布局文件就相当于设计图，Activity作为画家需要将成品按照设计图一层层画到画布上。  
给个图来表示：  
![ActivityWindow](/images/blogs/ActivityWindow.png)  

## View绘制与重绘过程
[Android应用层View绘制流程与源码分析]: http://blog.csdn.net/yanbober/article/details/46128379  
[Android视图绘制流程完全解析，带你一步步深入了解View(二)]: http://blog.csdn.net/guolin_blog/article/details/16330267  
[Android应用setContentView与LayoutInflater加载解析机制源码分析]: http://blog.csdn.net/yanbober/article/details/45970721

`inflate`只是相当于给画作打了个线稿，规定了要画的内容的层次关系，具体画什么、在哪里画，就是View的绘制过程。Android中为View和ViewGroup设计了一致的绘制流程。绘制视图总要有个开端，仍以Activity为例，前面说到Activity通过`setContentView`用Inflater把xml导入成View树，这时候我们只是在逻辑上得到了View的结构，还没有实际画到屏幕上。猜想Activity载入了View之后，应该是在某个生命周期的时候绘图，回想Activity总是在onStart之后才能看到Activity内容，跟随Activity的生命周期，猜想是某个Manager比如ActivityManager在控制。随便找了一下Activity的startActivity方法，发现Activity中所有的startActivity实际上都是调用的`startActivityForResult`方法（它有两个重载），它里面又是调用的`Instrumentation.execStartActivity(this, mMainThread.getApplicationThread(), mToken, this,intent, requestCode, options)`，在这个方法中使用`ActivityManagerNative`的`getDefault().startActivity()`真正开始Activity，查看源码知道`getDefault()`得到的是`ServiceManager.getService("activity")`，是系统提供的名为`activity`的服务，它经过`asInterface`转换成`IActivityManager`接口后作为结果返回，显然它的实现在Server端，同时有内部类`ActivityManagerProxy`用来调用远程服务，前面分析AIDL和Binder机制时候说过，实际上对远程服务的调用中，系统Binder驱动和Server各持有一个服务的实例，其中系统持有的是代理。通过`queryLocalInterface`区分是从服务端还是客户端进行服务调用，从服务端调用就直接返回本地服务了，从客户端调用则通过代理来访问服务。  

这里暂时停下，看到有一个`ActivityManager`类，很有意思，它的内部类AppTask有个`startActivity`方法，首先获取`ActivityThread`，再通过`Instrumentation.execStartActivityFromAppTask`在`ActivityThread`的`ApplicationThread`上开始Activity，点进去一看它实际上是调用的参数中的`IAppTask.startActivity`，回来看传入的参数是`mAppTaskImpl`，它的值由构造函数确定，而AppTask的构造函数是隐藏的，这很Android。顺藤摸瓜发现它在ActivityManager的getAppTasks方法中调用，实际上是从`ActivityManagerNative.getDefault().getAppTasks(mContext.getPackageName())`得到的任务列表，好嘛，又看到`ActivityManagerNative`了，可问题在于，这货的AppTask又是从哪儿得到的呢？  

以上花了不少力气，结果只看明白了Activity的启动是系统服务负责的，虽然通过查看SDK源码的`ServiceManager`找到了启动Activity的系统服务，但是仍然没有看到控制Activity绘制内容的相关代码，解耦解得厉害就是有这点不好，全都是面向接口了不太好找实现。用了一个土方法，根据分包直接去找activity服务的实现代码，感谢Android良好的模块设计，很快就找到了`com/android/server/am/ActivityManagerService.java`，一看它继承了`ActivityManagerNative`，稳了。直奔`startActivity`方法发现原来用的是`startActivityAsUser`方法，它又调用的`ActivityStackSupervisor.startActivityMayWait`，这个方法挺长的，关键的一步是调用`startActivityLocked`，里面调用`doPendingActivityLaunchesLocked`，它又调用`startActivityUncheckedLocked`，(一路各种设置flag，最后调用了`ActivityStack`的`startActivityLocked`，在Activity当前栈栈顶添加一个`ActivityRecord`，再调用`WindowManager.addAppToken`在task中存下Token，并且记录<Binder,Token>对应关系，有空单独写个Activity的启动，这里就不再说了)，惭愧看很久也没看到开始Activity的具体代码。求助Google，然后得知`ActivityThread`(同样是`@hide`，在SDK中看不到)是启动Activity的关键类，它运行`performLaunchActivity`和`handleResumeActivity`来创建和启动Activity。看到`performLaunchActivity`的第一个参数是`ActivityClientRecord`类型，感觉自己前面的功夫没有白费，在这里连接起来了。这里通过Instrumentation(`android/app/Instrumentation`)依次创建了`Activity`、`Application`和`Context`，然后又依次调用了Activity的`OnCreate`、`performStart`、`OnRestoreInstanceState`、`OnPostCreate`。现在还没有调用`OnResume`，理论上应该还没有绘制内容。`performStart`方法也只是使用`Instrumentation`调用了Activity自身的`OnStart`，并且将Start事件分发到`mFragments`而已。  

来看`handleResumeActivity`，上来先是一记`performResumeActivity`(一番状态设置之后调用`activity.performResume`)，一堆状态读取和设置后，`if(r.activity.mVisibleFromClient)r.activity.makeVisible()`，这个方法看起来很有希望就是我要找的方法！找去源码一看，方法内容出奇的简单：`mDecor.setVisibility(View.VISIBLE)`可以说就这么一句。靠！根本还没完！  
___
接着看，原来`View`的`setVisibility`所调用的`setFlags`方法会在传入参数为`VISIBLE`时调用`invalidate(true)`方法(顺便一提`setFlags`方法当然也会处理`GONE`和`INVISIBLE`，再顺便一提这就解释了为什么设置可见性后不需要调用`invalidate`就可以自动重绘)。最终还是走到了View源码。  

打开`invalidate(boolean invalidateCache)`，调用的`invalidateInternal(int l, int t, int r, int b, boolean invalidateCache, boolean fullInvalidate)`，这里出现了一个神奇的`mGhostView`，之后还记得的话再来看看它是干嘛用的。`l t r b`是需要invalidate的区域，`invalidate`方法传入的直接就是View实例的整个区域。调用`mParent.invalidateChild`来invalidate给定区域。`mParent`只是一个`ViewParent`接口，它具体是指向什么实例还是要看代码。想一想Activity中什么内容都不设置的话，是有一个`DecorView`和一个id为content的`FrameLayout`，应该可以在`FrameLayout`或者`DecorView`找到具体的实现代码。幸运的是，在`FrameLayout`的父类`ViewGroup`中找到了`invalidateChild`方法(`ViewGroup`实现了`ViewParent`接口)。  

方法挺长，拣重点说。parent变量初始化为`ViewGroup`自身，先标记出dirty区域(为`transformMatrix`变形后的dirty区域上下左右各扩大0.5f)，然后调用parent的`invalidateChildInParent`方法对dirty区域进行扩展并将返回值赋予parent，如此往复直到parent为`null`为止。`ViewGroup`的这个方法返回`mParent`成员变量，它是一个隐藏的变量，从它的名字和可能为`null`来推断应该是会随着这个方法的调用而变化(我猜它会**层层向上**直到根视图`DecorView`的父`ViewRootImpl`，这个东西之前没有提过，其实它就是最顶层`DecorView`所添加到的地方, `DecorView`实例作为一个成员变量`mView`存在于它的实例中, 它受到`WindowManager`的实现类`WindowManagerImpl`实际上是`WindowManagerGlobal`的管理，它是逻辑上存在的一个东西，类型也不是View，但实现了`ViewParent`接口)，且是受系统什么服务控制的，我猜是`WindowManager`。在这个过程中如果如我所猜是层层向上，那么总会到达`DecorView`，而`DecorView`的父就是并不属于View子类的`ViewRootImpl`，当此时就会调用到`ViewRootImpl`的`invalidateChildInParent`方法，有意思的是这个方法最终返回`null`，这从侧面验证了我的猜测可能是对的。  

这里有一个关键变量，即`mAttachInfo.mInvalidateChildLocation`，这是一个仅两个元素的整型数组，分别存储child的left和top，从名字看存的是要invalidate的child的位置，真正绘制应该跟这个数组有关。
 
至此我发现前面整个过程是一个计算dirty区域的过程，想想图形的出现和更新其实都可以看做是对特定区域的绘制，通过计算dirty区域就统一了这两个行为。到了`ViewRootImpl`总算是计算完了dirty区域，接下来总应该画图了吧。一看果然，在`ViewRootImpl`的`invalidateChildInParent`方法中调用了`invalidateRectOnScreen`，而这个方法里又有个`scheduleTraversals`方法的调用，看起来就像是要遍历重绘的意思，它的代码是往Looper队列里添加了一个同步屏障，并在`mChoreographer`里添加了一个`TraversalRunnable`的回调，跑去这个Runnable的定义一看，就是调用了`doTraversal`方法，方法内容是移除同步屏障并执行`performTraversals`方法。这样想来，估计窗口第一次显示的时候类似的遍历过程会在`WindowManagerGlobal`调用`addView`方法之后调用吧。  

先不去管他，咱们来看`performTraversals`方法。……………………好长，里面写了一些debug的print，有空的话可以把debug开关打开来profile一下。这里挑重点看：首先是判断了一番当前的状态，是否需要创建Surface，是否需要完全重绘，是否需要硬件渲染，根据这些状态设置相应的条件，在满足Measure的条件时，调用`performMeasure`进行测量(该方法的内容是调用`mView.measure()`，在`View`的该方法实现代码中，调用了`onMeasure`，而`DecorView`的`onMeasure`方法在计算好自身宽高以后，调用了父类的`onMeasure`方法，我们还记得`DecorView`的父类是`FrameLayout`，去查看它的`onMeasure`方法就会发现，它对每一个child调用了`measureChildWithMargins`进行**递归measure**，然后才调用`setMeasuredDimension`设置自己的宽高)，measure不好还有可能measure第二次，测量完了之后`layoutRequested=true`； 接下来就执行了`performLayout`进行布局，方法中首先调用host(顶层`mView`的缓存)的layout进行布局(仍然调用的父类的`FrameLayout.onLayout`方法，非常暴力，就是一个`layoutChildren`进行**递归layout**)，然后计算有效的布局请求的View数量，如果此时还有需要布局的View则执行以下三步：遍历调用`View.requestLayout`，接着调用`measureHierarchy`，然后是`host.layout`再走一遍。这三步是第二次layout了，此时再检查还有没有需要布局的View，如果还有就只好将遍历调用`View.layout`的任务post到队列中等待下一帧执行，该方法完成后源码中注释说此时所有View就已经计算好并放置好了，接下来就会计算透明区域，主要就是把透明区域搜集起来并设置，如果透明区域跟之前的不同，则要求完全重绘；此时已经知道了屏幕上哪些地方需要重绘，哪些地方是透明的，接下来判断是否cancelDraw，`mAttachInfo.mTreeObserver.dispatchOnPreDraw()`完成(`AttachInfo`是`View`的内部类，当某个`View`附着到它的父窗口时，这个类的实例就存储一些相关的信息)或者`viewVisibility!=VISIBLE`都会被记为`cancelDraw`，如果可见性为`VISIBLE`则此时还会再试一次`scheduleTraversals`。只有在经过前面的状态判断后仍然需要绘制的情况下，才会调用`performDraw()`进行绘制，该方法调用`draw()`方法，在`draw()`中首先`mAttachInfo.mTreeObserver.dispatchOnDraw()`将事件传给观察者(`dispatchOnDraw`方法中，依次调用已注册的`OnDrawListener`中的`onDraw()`方法)，然后根据情况调用`mAttachInfo.mHardwareRenderer.draw`或者`drawSoftware`(在`drawSoftware`中会调用`mView.draw`，查看`DecorView`源码知道其实主要是调用的`super.draw`，再看`FrameLayout`源码发现未被重写，再往上到`ViewGroup`的源码，很棒仍然没找到`draw`不过找到了`drawChild`，再往上看，就到`View`了，`View`的`draw`方法中会先调用`drawBackground`，再调用`onDraw`来画自身内容，然后调用`dispatchDraw`请求孩子进行绘制，最后调用`onDrawForeground`。然而`View`中`dispatchDraw`是个空方法，根据逻辑它应该是个`ViewGroup`来实现，于是回来找到`ViewGroup`，果然里面有`dispatchDraw`的实现代码，这个方法大体还是对每个child调用`drawChild`，`drawChild`方法只是返回`child.draw`的结果，至此完成**递归draw**)，这期间有可能再来一次`scheduleTraversals`。  

看到这里有个疑问，`dirty`区域哪去了？其实回头一看就会知道，`dirty`早在`invalidateRectOnScreen`的时候就被合并到`mDirty`中了，而`mDirty`会在`draw`方法中使用。想必各位也注意到了，只有`draw`是要先`dispatchOnDraw`的，这暗示了这三个重要的步骤的逻辑是前两者由孩子发起，到父亲结束，而绘制动作是由父亲发起。但三个步骤的实际执行都是先执行孩子的对应方法，才执行父亲的对应方法。回想一下，一切的开端都在View调用`invalidate(true)`的时候。至此，整个重绘的过程就已经清楚了。  

那么，当一切开始时，Activity是怎样从零开始绘制`DecorView`的呢？借助前面重绘过程的探索分析，我发现了在执行`ActivityThread#handleResumeActivity`时会调用`r.activity.makeVisible()`方法使`activity`可见，这个过程就是上面我们分析过的测量和绘制视图的过程了。那么这些视图是什么时候初始化的呢？从`setContentView`到视图可见，之间经历了什么？其实在`makeVisible()`这个调用之前，在`performResumeActivity`之后，还有一个重要的步骤，就是添加`DecorView`，具体是先通过`r.window = r.activity.getWindow()`获取Activity所在的Window，并记录到r中，一般来说这个Window就是`PhoneWindow`了，所以接下来就不难理解调用`r.window.getDecorView()`获取到了`DecorView`的引用。获取到`DecorView`引用后，首先设置为`INVISIBLE`，然后通过`ViewManager wm = a.getWindowManager()`获取到`ViewManager`实例，通过`WindowManager.LayoutParams l = r.window.getAttributes()`获取到布局参数，最后通过`wm.addView(decor, l)`添加`DecorView`到`WindowManager`中。  

来看`addView`，前面讲过这里的`WindowManager`实际上是`WindowManagerGlobal`，所以我们知道这里实际调用的是`WindowManagerGlobal.addView(View view, ViewGroup.LayoutParams params, Display display, Window parentWindow)`(这里的后两个参数来自于`WindowManagerImpl`的`mDisplay`和`mParentWindow`)，它做了什么呢？打开源码，看到它首先使用`params`参数调整父窗口的子窗口(`parentWindow.adjustLayoutParamsForSubWindow(wparams)`)，然后**同步地**先确保之前的`removeView()`执行完毕，再用`View.getContext()`实例化了`ViewRootImpl`并赋值给局部变量`root`，同时分别在`mViews mRoots mParams`添加记录`view root params`。同步块之后调用了`root.setView(view, params, panelParentView)`设置视图内容。这个`setView`方法厉害了，它整个都是同步的，跳过一些我暂时看不懂的`SurfaceHolder`等内容后，首先设置`mAttachInfo.mRootView = view`，然后调用了`requestLayout`，然后调用`mWindowSession.addToDisplay(mWindow, mSeq, mWindowAttributes, getHostVisibility(), mDisplay.getDisplayId(), mAttachInfo.mContentInsets, mAttachInfo.mStableInsets, mAttachInfo.mOutsets, mInputChannel)`将`mWindow`加入到`WindowManagerGlobal`中，然后`view.assignParent(this)`把`ViewRootImpl`注册为`DecorView`的父亲。其中`mWindowSession`是`WindowManagerService`的远程事务。至此，`DecorView`就已经被加入到`ViewRootImpl`并随着`PhoneWindow`加入到了`WindowManagerService`中接受其管理。  

总结一下，可以看到`View`的绘制过程和重绘过程其实本质上是一样的，只不过绘制的dirty区域是整个窗口。在`Activity`初始化的时候，在`ResumeActivity`之后，真正显示之前，`ActivityThread`进行了Activity中DecorView的创建、初始化和添加，然后通过调用`setVisibility`来间接调用`invalidate`方法进行重绘，在重绘时子View通过调用`invalidate`方法，请求父亲计算dirty区域并递归依次按需执行`measure、layout、draw`三种方法。孩子节点要进行重绘的时候，只需要在设置好新的界面值之后直接或间接调用`invalidate`方法，就可以触发该孩子节点及其所有**祖先**的重绘。源码为了健壮性，有很多地方都可能进行两次甚至三次某个步骤，有些地方甚至直接重新全部执行，过多次数的计算和重绘可能会产生性能瓶颈。  

## View与ViewGroup事件分发  
[Android触摸屏事件派发机制详解与源码分析一(View篇)]: http://blog.csdn.net/yanbober/article/details/45887547  
[Android触摸屏事件派发机制详解与源码分析二(ViewGroup篇)]: http://blog.csdn.net/yanbober/article/details/45912661  
[图解 Android 事件分发机制]: http://www.jianshu.com/p/e99b5e8bd67b  

在前面我们已经见到了两个神奇的方法`dispatchOnPreDraw`和`dispatchOnDraw`，这一节我们来看更多的`dispatchXXX`, 也就是Android中的**事件分发机制**。多一句嘴, View的事件分发机制只是针对触屏事件的一种处理思路, 这种思路完全可以用在别的事件的分发和处理流程上.  

我们知道, 对于用户来说, 跟Activity交互的主要方式之一就是触屏, 而展示在用户面前, 用户可以看见的东西, 就是我们的 `View` 和 `ViewGroup`, 所以 `View` 和 `ViewGroup` 必然会对触屏事件做处理. 这里我们直接通过读源码的方式去了解`View`的时间分发机制, 并着重根据以下几种触屏事件来梳理思路, 加深理解:  

1. 点击事件(Click)分发  
2. 按下事件/释放事件  
3. 滑动事件  

同时, 我们需要注意一个事件的处理周期, 即它分发的路径是怎样的, 是否会停止分发, 满足什么条件会停止分发.  
对事件分发机制有充分理解, 那么各种按键事件响应处理和事件冲突问题, 就可以迎刃而解.  
写这一节参考了网上的一些资料, 这里先做个说明, 本节不会以"消费"来表示事件处理函数返回`true`的情况, 纯粹因为我个人不喜欢这个词, 觉得表意不明确, 我认为方法的返回值只是表示"是否停止处理事件", 而这个意思用"消费"来表示不太准确, 尽管源代码中用来表示这种情况的`Consume`通常确实被翻译为消费. 各位读者如果习惯这样称呼, 也并无不妥. 其他内容如有引用会在文中注明.  
本节行文方式是先背景知识, 再总体流程图, 再贴源码详细解释.  

### 事件产生  
触屏对操作系统提供硬件接口用以通知操作系统产生了什么事件, 操作系统通过硬件驱动读取和解析硬件接口的事件, 这个跟源码关系不大我们就不说了. 我们只说当事件被Android系统捕捉到之后, 传入Activity的时候是个什么形式, 没错, 就是`MotionEvent`. 硬件输入事件被`InputEvent`来描述, 该类是一个抽象类, 其有两个直接子类, 即`MotionEvent`和`KeyEvent`, 分别对应触屏事件和按键事件. 我们这里重点关注`MotionEvent`.  
它在`frameworks/base/core/java/android/view/MotionEvent.java`中定义, 比较大, 注释也多, 我们只抽象地简单了解一下它刻画了触屏事件的哪些属性.  
首先它定义了一系列ACTION常量来表示触屏事件, 例如我们下面会用到的`ACTION_DOWN` `ACTION_MOVE` `ACTION_UP`, 以及`ACTION_OUTSIDE` `ACTION_CANCEL` `ACTION_POINTER_DOWN` `ACTION_POINTER_UP` `ACTION_SCROLL`等.  
其次它还定义了触屏事件的坐标属性包括偏移量等信息, 对应的处理和转换方法(@hide), 以及相应的Getter.  
最后它定义了触屏事件的发生时间, 以及一系列`toString`方法.  
这里面有很多方法都是native的, 而且也用到了aidl可见跟binder驱动估计也有关系. 如果今后有时间, 我会深入去看一下硬件到事件的过程, 今天先点到为止.  
对`MotionEvent`本身感兴趣的同学可以去看看源码和官网的文档, 也可以在网上搜一下, 已经有很多人写过相关的博客.  

### 事件分发和处理  
事件产生之后(`MotionEvent`), 是怎么分发和处理呢? 我们先来看一张图:  
![事件分发总流程](/images/blogs/view_event_dispatch.png)  
上图来自[图解 Android 事件分发机制]. 图中白色箭头代表事件流动的方向. 大体上来说一个全新的事件就是按照这个过程分发和分级处理的.  
可见事件处理的主体主要分三个层级, 即`Activity` `ViewGroup` `View`, 你没看错, `Activity`这个浓眉大眼的家伙也可以处理`MotionEvent`.  
网上常说"从上往下传递事件, 从下往上处理事件", 其实默认是从"View树层次结构"的角度去说的, 而不是"View的显示层次", 在"View树层次结构"这个角度上, `Activity`及其`DecorView`就是最上层, 逐层往下是`ViewGroup`和`View`, 这其实是一个**深度优先搜索**的过程, 对于一个确定的`MotionEvent`, 在这棵树上的路径也是确定的. 其实每一层都是先往下搜索看子节点是否处理完毕这个事件, 如果处理完毕了, `dispatchTouchEvent`就返回`true`, 否则本层调用`onTouchEvent`进行处理并返回处理结果.  
所以我们可以猜测代码逻辑是这样:  

```
public boolean dispatchTouchEvent(MotionEvent e){
    View target;
    for (View child: children){
        target = child.getTouchTarget(e); // find event target
        if (target != null){
            break;
        }
    }
    if (target != null && target.dispatchTouchEvent(e)){
        return true;
    }
    
    return onTouchEvent(e);
}
```  

当然事实上远没有这么简单, 在`View`和`ViewGroup`中, 在实际分发和处理事件之前, 进行了一系列的逻辑处理(这里我就不贴源码了, 有兴趣的同学可以自行查看), 如: 在`ViewGroup`中, 判断当前是`DOWN`事件或者目标非空, 如果满足条件, 则设置一系列标记, 并判断该事件是否被自己拦截(`onInterceptTouchEvent`). 如果不满足条件, 即非`DOWN`事件且目标为空, 则直接标记该事件被拦截, 随后关键步骤是调用`dispatchTransformedTouchEvent`, 根据传入参数来决定是调用孩子节点的`onTouchEvent`还是调用`super.dispatchTouchEvent`(即`View.dispatchTouchEvent`, 这条路径会让自己的`onTouchListener`或`onTouchEvent`处理事件); 在`View`中, `DOWN`事件会让`View`停止滚动, 判断是否被`OnTouchListener`处理完毕(该步取代了`dispatchTouchEvent`, 原因显而易见), 如果没有则调用`OnTouchEvent`处理; 以上两者都会判断事件是否满足一致性校验, 是否满足安全性策略, 是否被取消.  
与此相比, `Activity`中的事件处理就是一股清流, 它的代码非常简短, 几乎就跟我上面给出的差不多. 它先将`DOWN`事件交给`onUserInteraction`处理, 然后并不返回, 继续将该事件交给`getWindow().superDispatchTouchEvent`, 如果未被处理完毕则交给自身的`onTouchEvent`处理并返回.  
神秘的就在于`superDispatchTouchEvent`了, 复习一下第一节的知识知道它八成是在`PhoneWindow`中定义的. 打开一看, 果不其然, 又被`mDecor`代理了...我们回忆一下, `DecorView`这个内部类是继承自`FrameLayout`的, 也就是说他是个`ViewGroup`, 好, 我们来看看这个类中`superDispatchTouchEvent`怎么定义的:  

```  
public boolean superDispatchTouchEvent(MotionEvent event) {
            return super.dispatchTouchEvent(event);
}
```  

直接丢给父类了, 人干事? 好在我们惊喜地发现这个方法是`public`的, 我们可以自己定义`Window.superDispatchTouchEvent`. 当然, 最惊喜的是`getWindow()`也是`public`的, 使得自定义`Window`并修改事件分发策略成为可能, 这个设计绝赞.  

现在我们已经大体知道了事件分发和处理的逻辑, 不清楚读者心中产生了什么新的疑问, 我有两个疑问: 1. 事件从何处传递给`Activity`(我们怎么确定顶层处理组件是`Activity`而不是其他什么东西?); 2. 事件何时终止传递, 连续事件(`DOWN->MOVE->UP`)有时候是要同一个`View`进行处理的, 例如滑动`ListView`, 如何保证这个过程?  

对于第一个问题, 我自己想想就觉得头大, 首先它肯定是硬件传递给我们操作系统再分发给某个服务(`ActivityManagerService`?)的, 这里面估计涉及到`Binder`通讯, 分析`Binder`原理的时候要了我半条命(感兴趣的同学可以在我博客里搜索到`Binder`原理解析的文章). 其次, 对于裸的驱动事件, 必然需要借助设备驱动将事件进行重新包装和定义, 这一步可能是`Android`提供接口, 设备驱动来完成, 也可能是`Android`系统自己来完成, 具体实现过程不太好找. 没办法, 有此疑问必然辗转反侧, 所以还是来看看吧. `Window.Callback`接口类定义了`dispatchTouchEvent`这个接口, 但是对于`Activity`来说哪个调用了它的这个方法, 我一点线索也没有. 回忆应用启动过程, 应该可以从`MotionEvent`的构造方法找到一点蛛丝马迹. 具体艰辛的过程我就不细述了, 总之最后终于发现了如下流程:  
`Linux`系统已经实现了对触屏事件的封装和传递, 硬件事件将会通过内核底层的硬件驱动进行处理, `Android`系统在此基础上进一步封装内核传递过来的触屏事件, 并根据事件的具体值, 依据硬件设备协议, 来初始化触屏事件`MotionEvent`.(回忆`adb sendevent`其实传输的就是触屏事件或按键事件)  

然后事件被包装成`QueuedInputEvent`, 并且由被设置了`Callback`的`Message`携带, 进入了主线程`ActivityThread`的消息队列, 处理时直接`handleCallback`调用了`Callback`, 而这个`Callback`是调用`ViewRootImpl.dispatchInputEvent`(接口来自`InputEventReceiver`)分发给了`ViewRootImpl`由`ViewRootImpl.onInputEvent`处理, 它才是真正意义上第一个处理触屏事件的类, 在该类中经过一系列处理后会传递到以下代码中:  

```  
private int processPointerEvent(QueuedInputEvent q) {
    ...
    final MotionEvent event = (MotionEvent)q.mEvent;
    final View eventTarget =
            (event.isFromSource(InputDevice.SOURCE_MOUSE) && mCapturingView != null) ?
                    mCapturingView : mView;
    mAttachInfo.mHandlingPointerEvent = true;
    boolean handled = eventTarget.dispatchPointerEvent(event);
    ...
}
```  

至于`mView`其实就是`DecorView`的实例, 是整个`View`树中最上层的可见元素, 见上文. 于是在这里调用了要么是`mCapturingView`要么是`mView`的`dispatchPointerEvent`, 而在`View.dispatchPointerEvent`的实现是, 如果它是`MotionEvent`, 则调用`dispatchTouchEvent`.  
在`DecorView.dispatchTouchEvent`中, 如果有`Callback`调用`Callback.dispatchTouchEvent`, 否则调用`super.dispatchTouchEvent`. 这就厉害了, 这里对于`Activity`来说, 它是实现了`Callback`接口的, 也就是说这里如果传入的`Callback`是`Activity`实例, 那么就是直接调用`Activity.dispatchTouchEvent`了, 一切都解释得通了.  
来梳理一下这一段的调用:  

```  
ActivityThread.mH.handleMessage(Message msg) ->  
ActivityThread.mH.handleCallback(msg.callback)  
ViewRootImpl.dispatchInputEvent(q) ->  
ViewRootImpl.onInputEvent(mEvent) ->  
ViewRootImpl.processPointerEvent(q) ->  
DecorView.dispatchPointerEvent(q.mEvent) ->  
DecorView.dispatchTouchEvent ->  
Activity.dispatchTouchEvent ->  
Activity.getWindow().superDispatchTouchEvent ->  
DecorView.superDispatchTouchEvent ->  
FrameLayout.dispatchTouchEvent  
```  

可以看到最后好像是绕了个圈, 从`DecorView.dispatchTouchEvent`出发, 又回到`DecorView.super.dispatchTouchEvent`.  

对触屏事件的定义见代码`./bionic/libc/kernel/common/linux/input.h`.  
触屏驱动见驱动代码目录`./kernel/goldfish/drivers/input/touchscreen/`.  
事件封装见`./frameworks/base/services/input/EventHub.cpp`.  

(如果有兴趣, 可以沿这条路径查阅代码, 感受一下我的艰辛: `MotionEvent.obtain() -> ./frameworks/base/services/input/EventHub.cpp` -> 实在找不到触发点, 写了个Demo打印调用栈, 发现是异步调用, 事件被放进`ActivityThread`的`looper`中 -> 我能怎么办, 我也很绝望啊, 事件从哪里放进looper中的? 沿着处理栈看看吧 -> 惊喜发现)  

至此, 第一个疑问基本得到解答. 第二个疑问是, 事件何时终止传递, 对于连续事件如何处理? 事实上, 从以上代码可以看到, 当任意一层`dispatchTouchEvent`提前返回而无论自身还是父亲节点都不再调用下一层`dispatchTouchEvent`的时候, 就终止事件传递了, 具体来说就是`dispatchTouchEvent`在调用孩子节点的`dispatchTouchEvent`之前就返回了`true`(例如, 被拦截或者传递给`onTouchEvent`并返回`true`). 连续事件分两类, 一类是像`ACTION_MOVE`这样的, 由一系列连续的事件点构成的一个事件, 另一类就是像我上文提到的那种, 按下不放滑动手指.  

对于第一类, `Android`系统实际上将这种多次事件封装在了一个`MotionEvent`中, 并且用`ACTION_MOVE`这种标记来表示, 实际上我们手指触摸屏幕的时候, 很难达到仅有`ACTION_DOWN`->`ACTION_UP`, 中间多少都会夹几个`ACTION_MOVE`.  

对于第二类, 其实也很简单, 在`DOWN`事件发生时, "**ACTION_DOWN在哪个View的onTouchEvent返回true，那么ACTION_MOVE和ACTION_UP的事件从上往下传到这个View后就不再往下传递了，而直接传给自己的onTouchEvent 并结束本次事件传递过程... 如果ACTION_DOWN事件是在dispatchTouchEvent消费，那么事件到此为止停止传递，如果ACTION_DOWN事件是在onTouchEvent消费的，那么会把ACTION_MOVE或ACTION_UP事件传给该控件的onTouchEvent处理并结束传递。**"(以上引用自[图解 Android 事件分发机制]).  
现象好像确实如此, 但是代码是怎么做到的呢? 带着好奇我重新看了`ViewGroup`和`View`的事件分发, 发现了实现方法. 对于`View`, 如果在`dispatchTouchEvent`时调用了默认的`onTouchEvent`, 并且它是可点击的, 那么`DOWN` `UP` `MOVE` `CANCEL`四个事件会进入该函数的`switch`, `switch`之后必然返回`true`, 即触屏事件被处理完毕. 由于`View`本来就是事件分发的最底层, 所以它并没有显示后续事件拦截是如何实现的. 对于`ViewGroup`, 由于它并没有重写`onTouchEvent`方法, 故所有事件如果传递到了`onTouchEvent`则必然也是走`View`的方法. 在`dispatchTouchEvent`过程中, 特殊处理`DOWN`事件, 此时会清除掉内部保存的`target`链表, 并遍历一个有序的`View[] children`数组(按绘制先后顺序排序), 对于每一个孩子节点, 在`target`链表中寻找是否存在它对应的`target`, 如果存在则循环结束跳出, 否则对这个孩子调用`dispatchTransformedTouchEvent`, 并在返回`true`时构造这个孩子的`target`并插入到链表头部, 然后结束循环, 以上内容都是仅在`DOWN`事件时发生. 这就意味着, 如果一个`child`在`DOWN`事件时`dispatchTransformedTouchEvent`返回`true`了, 那么它在链表中就应该存在对应的`target`. 用`newTouchTarget`这个变量来暂时存储, 而且这个`target`应该在链表靠头部的位置. 如果`newTouchTarget`为空(意味着`dispatchTransformedTouchEvent`返回`false`)但`target`链表不为空(意味着本`ViewGroup`中已经有一组`target`等待接收事件), 则`newTouchTarget`存储链表尾节点. 循环结束后, 根据`target`链表继续处理事件, 如果`target`链表为空说明本`ViewGroup`的孩子节点没有曾经将事件处理完毕过(一旦有过, 就会被链表保存)或者本次事件是`DOWN`事件但被取消或拦截了, 那么调用`View.dispatchTouchEvent`继续处理事件, 而该方法默认是调用自身的`onTouchEvent`. 如果`target`链表不为空, 遍历`target`链表把事件传给对应的`View`进行处理(划重点!!**事件如果被标记为取消或者拦截, 则不继续传递给孩子节点, 事件要么由自身`onTouchEvent`处理, 要么传递给已经在target列表中的节点处理; 事件不被取消也不被拦截的情况下, 如果孩子节点有一个返回true, 则以后的非DOWN事件也只交给target列表中的节点处理, 不再继续传递给其他孩子; target列表中的节点则是调用其dispatchTouchEvent方法继续传递和处理事件**), 有任意一个处理完毕则都标记本`ViewGroup`已处理完毕该事件(但是不会中断遍历). 在该过程中如果有`View`标记为下次`UP`事件时取消, 则同时从链表中删除并回收该`target`.  

小结一下, 借助`target`链表, `ViewGroup`记录了处理`DOWN`事件的`View`集合, 并在下次其他事件传入时仅传给这个链表中的`View`, 这就实现了对连续事件的处理. 而且对于`View`的`onTouchEvent`默认实现来说, 四种事件均会返回`true`, 即`View`本身默认就是会拦截事件的.  

[android 电容屏（二）：驱动调试之基本概念篇]: http://blog.csdn.net/xubin341719/article/details/7833277  
### 事件冲突  
仔细理解了上一节的内容之后, 就不难预见事件冲突这种情况发生的原因以及解决方案了.  
总的来说, 事件冲突的原因就是意料之外的`View`或者`ViewGroup`把事件拦截了, 从而我们想用来处理事件的`View`或者`ViewGroup`无法接收到事件.  

典型的, `ScrollView`中嵌套`ListView`, 冲突产生的原因是`ScrollView`在滑动拖拽的过程中会让`onInterceptTouchEvent`返回`true`, 直接导致`ScrollView`拦截事件, 不会被传递到深一层的`ListView`(其他`View`也一样). 解决方案也很简单, 就是重写`ScrollView`的`onInterceptTouchEvent`, 告诉`ScrollView`哪些情况该拦截, 哪些情况不该拦截. 如果不能修改这个拦截事件的`View`怎么办呢? 也好办, 从自己可控的`ViewGroup`出发, 调用`requestDisallowInterceptTouchEvent`要求该`ViewGroup`及其祖先节点不拦截事件, 再视情况恢复这个标记位.  
可以看出, 了解了`Android`系统各个层次对事件的分发处理和返回的策略之后, 遇到任何事件冲突问题我们都能够很容易找到症结所在并给出解决方案.  

对于嵌套滑动, `View`定义了一个接口`dispatchNestedScroll`用来传递嵌套滑动事件. 今后有空我将会继续研读这一块的设计.  
