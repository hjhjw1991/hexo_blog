---
layout: post
title: Mac下配置ssh访问远程私有git服务器
description: Mac下的SourceTree添加远程仓库只能从bitbucket和github两种中选择，怎么指定任意的git服务器远端及使用任意的ssh key呢?本文解决这个问题
category: blog
tags: [git,ssh,mac,guide]
date: 2015-08-30
---

## 0. 前言  
拉取远程仓库的前提是你在远程主机上保存有公钥，并且本地有私钥。昨天在mac上想要添加一个我自己git服务器的远程仓库，因为一直是用Win下的SourceTree，没想到mac下的SourceTree添加远程仓库只能从bitbucket和github两种中选择，而且必须要账号密码登陆，根本没有ssh登陆方式。无奈之下只好决定终端搞定再添加本地仓库。  
下面说一下命令行从自有git服务器拉取远程仓库的方法。  

## 1. 准备ssh key
如果没有ssh key，可以通过终端ssh-keygen产生一个，不细说。我这里是直接把win下的私钥拿过来用的。  
将私钥放到`~/.ssh/`下，如果不想每次输入`-i`指令指定使用哪个私钥的话，在`.ssh/`下新建一个config文件，内容：  

    #Host整块可以看作是一个你要识别的模式，对识别的模式，进行配置对应的的主机名和ssh文件  
    #其中Host属性指明你服务器所在的位置，可以使用IP，HostName可以不配置
    Host "project1"
    HostName "github.com" #要登录主机的主机名
    User “git” #登录名
    IdentityFile "~/.ssh/project1_rsa" #指明上面User对应的identityFile路径

    Host "project2"
    HostName "xxx.xxx.xxx.xxx"
    User “git”
    IdentityFile "~/.ssh/project2_rsa"
    
这样ssh就能自动为远程地址选择key文件了。  
在第一次使用的时候，可能会提示密钥too open，如下  

	@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	@        WARNING: UNPROTECTED PRIVATE KEY FILE!          @
	@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	Permissions 0777 for '/etc/ssh/ssh_host_rsa_key' are too open.
	It is recommended that your private key files are NOT accessible by others.
	This private key will be ignored.
	bad permissions: ignore key: /etc/ssh/ssh_host_rsa_key
	Could not load host key: /etc/ssh/ssh_host_rsa_key
只要把对应的key的权限改成只有创建者有权限就可以。700 600都行。

    chmod 700 ssh_key

## 2. 克隆远程仓库
这个比较简单，就是终端输入clone命令即可。  

    git clone ssh://ip.to.your.gitserver:port/reponame
    
如果有多个远程，怎么添加呢？git的remote命令提供了方法。  

    git remote add xxx URL
    git remote xxx set-url URL
    
remote的参数可以看git的手册。

## 3. reference
[GIT多个项目使用不同的公钥和私钥 ](http://zywhunter.blog.163.com/blog/static/64659941201402262514727/?COLLCC=2343345554&)  
[Git documentation](http://git-scm.com/docs/git-remote)
