---
layout: post
title: utf-8项目生成javadoc编码GBK的不可映射字符
description: jvm arguments -encoding utf-8 -charset utf-8
category: tips
tags: [windows,Java,]
date: 2016-2-23
---

原因是javadoc生成的时候没有指定编码格式，windows默认编码gbk识别不了utf-8的中文  
解决方法  
Eclipse中Generate javadoc最后一步中VM参数设置加入以下代码  

```java
-encoding utf-8 -charset utf-8  
```