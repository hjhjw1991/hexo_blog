---
layout: post
title: 【Android源码伴读】Layout那些事儿
category: android
description: android中layout可以通过xml和java代码两种方式来定义，那么它是怎么做到的？android中对于layout的属性概念是怎么定义的？绘制的时候又是怎么发挥作用的呢？ --施工中
tags: android
date: 2015-10-23
---

>前言：Layout的xml文件本质上也就是遵从某一命名空间schema的xml描述，其使用的xmlns命名空间的名字是android，如此而已，还有一些如tools和app的命名空间前缀。在android命名空间中，Layout是用于布局的概念，它拥有一些属性，它在android中是怎么发挥作用的？

## android:layout_xxx
简单来说，有layout前缀的属性，表示这个属性是一个针对所属layout的属性，它的值是被父容器所用的，而不是本容器。  
换句话说，带layout前缀的属性，是针对当前整个容器本身的，而不带layout前缀的属性，则是针对容器的内容而定的。如果是一个TextView，那么前者就是TextView本身，而后者就是TextView里面的text属性，也就是文字内容。  
一些易于理解及不易混淆的属性，例如layout_height和layout_width两个属性，就不详加说明了。下面以一个不太易理解的例子和一个易混淆的例子来说明android中这个layout前缀的含义。  
### layout_weight
weight的混淆来自于望文生义。诚然weight是代表权重的意思，是用于修饰layout的width和height的，但是它在和width、height两个属性一起使用的时候，会发生一些与我们第一印象的预期不太符合的现象。  
首先，它的正当用途就是指定所修饰的属性在父容器中对剩余空间的分配权重的，值越大权重越大，所分配的比例越大。在官方的指导中，是建议被修饰的属性其值设置为"0dp"，从而避免发生意料之外的行为。官方表示，会发生意料之外的行为是因为根据width值的分配过程与根据weight值的分配过程是两个独立的不同的过程，因此建议使用weight的时候仅使用weight。  
我们前面说过，weight表示的是对**剩余空间**的分配权重。
于是我们可以猜想，这个过程是在分配了其他明确知道大小的同级兄弟View之后才进行的过程。由此我们猜想，layout分配的时候，先计算确定大小的view，然后在剩余的空间中，按照权重分配空间给剩余view。  
基于这个猜想，我们有以下的考虑：  

- 如果从书写顺序上，带weight的view与确定大小的view交错书写，那么会发生什么呢？  
- 如果我们在书写的时候，被修饰的width和height指定了值，会发生什么？
- 我们可不可以对width和height指定不同的权重？weight是怎么找到它应该修饰的属性的？
- weight的值有什么限制吗？
- 如果我们猜测是错误的，那么真实过程是怎么样的？


我们接下来会做一系列的实验来猜想和验证weight的分配逻辑，给出这些问题的答案（除了最后一个，最后一个问题将放在Layout加载原理和源码中解析）。  

### layout_gravity
## padding margin align
## Layout及所有xml文件的加载原理
## Layout源码分析
## 自己动手写Layout
