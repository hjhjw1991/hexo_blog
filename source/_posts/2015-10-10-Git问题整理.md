---
layout: post
title: Git问题整理
description: Git使用过程中遇到的一些问题和技巧，方便排查
category: tips
tags: git
date: 2015-10-10
---

## 中文文件或目录名被转码
git默认会对路径里超过0x80的字节进行转码，设置`core.quotepath`为`fasle`可以禁止自动转码，详情见[git-config(1)][1]的`core.quotepath`部分。

	git config --global core.quotepath false

## fatal: cannot exec .. Permission denied
使用`git alias`命令`git st`(参考[这里的配置][2])时提示如下错误:

	fatal: cannot exec git st: Permission denied  

参考[stackoverflow][3]上的解答, 使用如下方法解决.  


- 安装strace

	`apt-get install strace`

- 使用strace执行`git st`

	`strace -f -e execve git st`

- 根据输出结果进行修正.

## Cygwin git ca-bundle.cert错误  
cygwin下用git向远程https仓库推送数据时遇到以下问题:  

	error: error setting certificate verify locations:
	CAfile: /usr/ssl/certs/ca-bundle.crt
	CApath: none while accessing https://github.com/wilbur-ma/dokuwiki-tweak.git/info/refs
	fatal: HTTP request failed
参考[这里][4]，解决办法是在cygwin上安装ca-certificates。

## recursion detected in die handler  
使用`git push`的时候遇到如下问题:

	fatal: recursion detected in die handler  

有时候也表现为：

	POST git-receive-pack (chunked) 

或者这样：  

	error: RPC failed; result=22, HTTP code = 411
	fatal: The remote end hung up unexpectedly
	fatal: The remote end hung up unexpectedly

这个问题非常常见，原因是<font color='red'>**http.postBuffer默认上限为1M**</font>所致，这算是一个bug，只有当remote是http(s)方式的时候会遇到。  
在git的配置里将`http.postBuffer`变量改大一些即可，比如将上限设为5M：

	git config http.postBuffer 5242880  

also @see [POST git-receive-pack (chunked) ][6]  
and  @see [git push fails: RPC failed; result=22, HTTP code = 411][5]

## 重写分支历史  
使用交互式变基`git rebase -i <target_commit_hash>`  

## reference  
[git aliases causing permission denied error][3]  
[git push fails: RPC failed; result=22, HTTP code = 411][5]  
[git-config(1) Manual Page][1]  
[POST git-receive-pack (chunked) ][6]  

[1]: https://www.kernel.org/pub/software/scm/git/docs/git-config.html
[2]: http://blog.atime.me/note/git-config.html#alias
[3]: http://stackoverflow.com/questions/7997700/git-aliases-causing-permission-denied-error
[4]: http://tech.idv2.com/2012/09/14/cygwin-git-error/
[5]: http://stackoverflow.com/questions/12651749/git-push-fails-rpc-failed-result-22-http-code-411
[6]: http://idas643.blog.163.com/blog/static/167104838201482810323961/

[hjhjw1991]:    http://hjhjw1991.github.io  "hjhjw1991"