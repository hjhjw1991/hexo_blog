---
layout: post
title: 从源码编译Android SDK
category: android
description: 从源码开始如何编译SDK
tags: [android,guide]
date: 2016-08-01
---

## Build Android SDK
前面已经编译好了系统源码，解决了一些问题，本以为编译SDK会顺顺利利一气呵成，我还是太天真了。  
上来就遇到`bluetooth-health`模块找不到。google之后知道这是官方预计删除但还没有删除bluetooth-health的Android.mk所致。解决方式是在`make sdk`之前先执行`lunch sdk-eng`。  
然后遇到`tools/base`目录找不到，退出，这时候都编译很久了，这不坑爹吗！解决方法是找到`sdk/eclipse/scripts/create_all_symlinks.sh`把里面所有有关`tools/base`的命令都注释掉。哦，这个文件在错误提示中有。顺便看了下这个`tools/base`是干嘛用的，好像跟Gradle构建有关，反正是Eclipse的东西吧，注释就注释了吧，顺便把`tools/swt`也注释了。  
再编译，漫长等待之后又遇到问题，`base:testutils`资源读不到。看了一下好像是测试相关的东西，好像是`easymock`依赖的东西，想了想果断也注释了。  
再编译又遇到`sdk/build/tools.atree`提示`couldn't locate source file`，我连`Package SDK`都看到了，你给我出个这个错误，我都不知道该不该修，感觉SDK已经打包好了……想想还是修吧。看了下路径下确实没有错误中提到的Template文件夹，更别说文件了。摸进`tools.atree`把载入的地方看了下，看不出来有什么用，也注释掉了。  
再编译，总算无错误完成。还不知道用起来有没有什么坑。  
