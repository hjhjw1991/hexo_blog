#!/usr/bin/env python
# -*- coding:utf-8 -*-

"""hjhjw1991 HexoBlog transform script
Use this script to transform articles from pure markdown in planning to formatted form in post.
Chinese characters from command line are not supported for now.

Usage:
    transform.py (-f|--file) INPUTFILE [-c=<category>] [-t=<tags>] [-d=<description>]
    transform.py -f your-article.md -c android -t android,guide

Python 3.5+ is required

Options:
    -h, --help                  使用帮助
    --version                   查看版本
    -f, --file                  指定输入文件
    -c=<category>               设置分类,目前的分类有{category}
    -t=<tags>                   设置标签,目前的标签有{tags}
    -d=<description>            设置文章简介[default: undefined]

Example:
    python transform.py -f INPUT.md -c android -t tag
    python transform.py -f INPUT.md -t [android,guide]
"""
import os
import sys
import log
import unittest
from log import LOGD, LOGV, LOGE
from post import RawPost, Post, rawPostToPost

Author = "HuangJun hjhjw1991@gmail.com"
VERSION = "0.1"


# resolve path
SRC_DIR = ""
DST_DIR = ""

log.TAG = os.path.basename(__file__)


# fill __doc__ with data
# check options, category, tag(space not allowed), description
# check output file(is it existing?)
# check input file(does it have a title? is it well-formed?)
# transform
# check if succeed
# cleanup, delete source


def fileToRawPost(fp):
    if not fp:
        raise ValueError()
    content = fp.read()
    rawPost = RawPost(content)
    return rawPost


def postToFile(post, parent=None):
    if not post:
        raise ValueError()
    validTitle = post.Title.replace(b" ", b"-")
    simpleFileName = '-'.join([post.Date, validTitle.decode()])
    unicodeFileName = simpleFileName + '.md'
    # unicodeFileName = '-'.join([post.Date, post.Title.replace(" ", "-")]) + '.md'
    if parent is not None:
        unicodeFileName = '/'.join([parent, fileName])
    if isWindows():
        fileName = unicodeFileName.decode('utf-8').encode('gb18030')
    else:
        fileName = unicodeFileName
    with open(fileName, 'w') as outfile:
        outfile.write(str(post))
    return unicodeFileName

def isWindows():
    import platform
    pf = platform.system()
    return pf == "Windows"

def cli():
    try:
        from docopt import docopt
    except ImportError as e:
        LOGE(e.message)
        exit(-1)
    try:
        arguments = docopt(__doc__, version=VERSION)
        filename = arguments['INPUTFILE']
        # todo 终端输入的中文内容未能正确输出
        category = arguments['-c']
        description = arguments['-d']
        rawTags = arguments['-t']
        tags = None
        if rawTags:
            rawTags = rawTags
            tags = map(lambda s: s.strip(), rawTags.split(","))
        with open(filename, 'r') as infile:
            rawPost = fileToRawPost(infile)
            builder = Post.Builder()
            builder.Category(category)\
                .Tags(tags) \
                .Description(description)
            post = rawPostToPost(rawPost, builder)
            print(postToFile(post))
    except IOError as e:
        LOGE("%s: %s" % (e.strerror, e.filename))


class TransformTest(unittest.TestCase):

    @unittest.skip("testCli")
    def testCli(self):
        sys.argv = [__file__, '-f', 'test.md',
                    '-c', 'testcategory',
                    '-t', 'testtag1, testtag2']
        cli()

    @unittest.skip("testRawPostFileToPostFile")
    def testRawPostFileToPostFile(self):
        with open('test.md', 'r') as infile:
            rawPost = fileToRawPost(infile)
            builder = Post.Builder()
            builder.Category('test').Tags(['good'])
            post = rawPostToPost(rawPost, builder)
            postToFile(post)

    @unittest.skip("testFileToRawPost")
    def testFileToRawPost(self):
        with open('test.md', 'r') as infile:
            print(fileToRawPost(infile))

def validPythonVersion():
    version = sys.version
    return version.startswith("3.")

if __name__ == '__main__':
    # testCli() # passed
    # testFileToRawPost() # passed
    # testRawPostFileToPostFile()
    if validPythonVersion():
        cli()
    else:
        LOGE("python 3.5+ is required")
