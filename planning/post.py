# -*- coding:utf-8 -*-

import re
from codec import encode, ENCODE
encoding = ENCODE.UTF_8


class Post:
    """define post template
    format is consist with yml
    based on Markdown, no 1-class headline
    fixed Header
    """

    _header = """---
%s
---"""
    _body = None

    @staticmethod
    def validate(vtype, vvalue):
        if vtype == 'date':
            matcher = re.compile('[\d]{4}-[\d]{2}-[\d]{2}')
            return False if matcher.match(vvalue) is None else True
        return True

    def __init__(self, layout, title, category, description, tags, date):
        head = """layout: %s
title: %s
category: %s
description: %s
tags: %s
date: %s""" % (layout, title, category, description, tags, date)
        self._header = self._header % head
        self._title = title
        self._date = date

    @property
    def Title(self):
        return self._title

    @property
    def Date(self):
        return self._date

    @property
    def Body(self):
        return self._body

    @Body.setter
    def Body(self, body):
        self._body = body.decode()

    @property
    def Header(self):
        return self._header

    def __str__(self):
        return "\n".join([self.Header, self.Body])

    class Builder:
        _layout = 'post'
        _title = 'undefined'
        _category = 'uncategorized'
        _description = 'undefined'
        _tags = []
        _date = '1970-01-01'
        _body = ""

        def __init__(self):
            pass

        def Layout(self, layout):
            if layout and Post.validate('layout', layout):
                self._layout = layout
            return self

        @encode(encoding)
        def Title(self, title):
            if title:
                self._title = title
            return self

        def Category(self, category):
            if category and Post.validate('category', category):
                self._category = category
            return self

        @encode(encoding)
        def Description(self, description):
            if description and self._description == 'undefined':
                self._description = description
            return self

        def Tags(self, tags):
            if tags and (isinstance(tags, list) or isinstance(tags, str)):
                if isinstance(tags, list):
                    tags = str([x.replace('[', '').replace(']', '') for x in tags])
                elif isinstance(tags, str):
                    tags = tags.replace('[', '').replace(']', '')
                self._tags = str(tags).replace('\'', '').replace('"', '')
            return self

        def Date(self, date):
            if date and Post.validate('date', date):
                self._date = date
            else:
                self._date = Post.Builder.getNow()
            return self

        @encode(encoding)
        def Body(self, body):
            if body:
                self._body = body
            return self

        def Build(self):
            post = Post(layout=self._layout,
                        title=self._title,
                        category=self._category,
                        description=self._description,
                        tags=self._tags,
                        date=self._date)
            post.Body = self._body
            return post

        @staticmethod
        def getNow():
            import datetime
            return str(datetime.date.today())


class RawPost:
    """define raw post written in pure Markdown"""
    _titleIndex = None
    _descriptionIndex = None

    def __init__(self, content):
        self._content = content
        if isinstance(content, str):
            self._lines = content.splitlines(True)
        elif isinstance(content, list):
            self._lines = map(str, content)

    def findTitle(self):
        ret = "undefined-" + Post.Builder.getNow()
        pattern = re.compile('#[\s]?([^#]+)\n')
        for i in range(len(self._lines)):
            if pattern.match(self._lines[i]):
                groups = pattern.findall(self._lines[i])
                ret = groups[0]
                self._titleIndex = i
                break
        return ret

    def findDescription(self):
        ret = "undefined"
        pattern = re.compile('^([^#\n]{1,255})[^#\n]*')
        for i in range(len(self._lines)):
            if pattern.match(self._lines[i]):
                groups = pattern.findall(self._lines[i])
                ret = groups[0]
                self._descriptionIndex = i
                if len(ret) > 100:
                    ret = ret[:100]+'...'
                break
        return ret

    def findBody(self):
        ret = []
        for i in range(len(self._lines)):
            if i == self._titleIndex:
                continue
            ret.append(self._lines[i])
        body = ''.join(ret)
        return body

    def __str__(self):
        return ''.join(self._lines)


def rawPostToPost(src=None, builder=None):
    if not src or not builder:
        raise ValueError("Missing Parameter")
    title = src.findTitle()
    desc = src.findDescription()
    body = src.findBody()
    post = builder.Layout(None) \
        .Title(title) \
        .Description(desc) \
        .Date(None) \
        .Body(body) \
        .Build()
    return post


def testValidate(vtype, vvalue, expect):
    assert expect == Post.validate(vtype, vvalue), "Not Passed"


def testFindTitle():
    post = RawPost("""# hello world
## Second head
### Third head""")
    print(post.findTitle())


def testFindDescription():
    post = RawPost("""# hello world
## Second head
### Third head
me is description而且有中文""")
    print(post.findDescription())


def testNewPostByBuilder():
    builder = Post.Builder()
    post = builder.Title("hello") \
        .Category("post") \
        .Tags(["git", "jekyll"]) \
        .Description("") \
        .Date(None) \
        .Layout(None) \
        .Body("Hello World") \
        .Build()
    print(post)


def testNewPost():
    post = Post(layout="post", title="", category="",
                description="", tags="", date="")
    post.Body = "good"
    print(post)


def testNewRawPost():
    content = """# test.title

this line should be description

test.output

如果有中文?

hahaha

end"""
    rawPost = RawPost(content)
    print(rawPost)
    return rawPost


def testRawPostToPost():
    rawPost = testNewRawPost()
    builder = Post.Builder()
    post = rawPostToPost(rawPost, builder)
    print(post)
    return post

if __name__ == '__main__':
    # testValidate('date', '2017-03-28', True)
    # testFindTitle()
    # testNewPostByBuilder()
    # testNewPost()
    #     testNewRawPost()
    # testFindDescription()
    testRawPostToPost()
