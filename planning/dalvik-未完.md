# 学习Android上的JVM--Dalvik

最初是想要学习Android插件化的相关技术，发现自己对ClassLoader的基础知识准备不足，于是来到Java看`ClassLoader`。这里用到的Java知识相对较少，对Dalvik的`ClassLoader`多一些，所以将本篇放到了Android标签下。  

先看看Java中class文件的格式：  
![class](images/blogs/class.png)

图片中用一个结构体来描述了class文件中应有的内容。具体含义分析可以看[邓凡平老师的这篇文章][深入理解Android之Java虚拟机Dalvik]  

## Java中的ClassLoader
`ClassLoader`主要作用就是载入我们编译好的字节码到虚拟机上面。在Java这边，类加载机制分为四个层次，每个层次都有各自的`ClassLoader`，查找和加载有先后顺序，在Custom ClassLoader的时候JVM会从最后加载的`ClassLoader`实例开始，去寻找当前要加载的类是否已经被加载，如果这个类没有被加载，就调用它父亲的`loadClass`方法加载这个类，这个递归查找和加载的方式通常称为双亲委托.

>这里多提一句Java的类加载机制. 在编译成class文件后，JVM启动的时候会从底向上检查类是否已经加载，再从顶向下加载类，从底向上一共经过四层load，分别是Custom ClassLoader，App ClassLoader，Extension ClassLoader，Bootstrap ClassLoader。分别加载自定义的ClassLoader(继承java.lang.ClassLoader)，加载CLASSPATH目录下的类和包，加载jre/lib/ext/目录下的类和包，加载jre/lib/rt.jar。其中后三种的ClassLoader不继承java.lang.ClassLoader，想想也是，rt.jar都没加载，根本连java.lang.ClassLoader这个类都还没有，所以肯定不是继承的它，事实上最后一种ClassLoader是C++实现的。后三种都可以通过JVM参数来指定要搜索目录。JVM在搜索目录下找到class文件之后，会逐层检查这个class是否已经被加载，一旦发现某个classloader加载了这个类，就认为已经加载完毕，保证每个类只被加载一次。我们在Java启动时会传给它一个主类, 事实上它就是在以以上顺序搜索这个名字的Class文件并加载, 随后运行其中的main方法.  
>class文件中存储了文件结构信息(包括文件格式版本等)，元数据(类的声明，常量池)和方法的信息。在JVM运行class文件的时候，以栈的方式存储方法帧，其过程原理就是计算机体系结构中学过的原理。我们可以通过javap命令查看class文件内容  


```
protected Class<?> loadClass(String className, boolean resolve) throws ClassNotFoundException {
    Class<?> clazz = findLoadedClass(className);
 
    if (clazz == null) {
        try {
            clazz = parent.loadClass(className, false);
        } catch (ClassNotFoundException e) {
            // Don't want to see this.
        }
 
        if (clazz == null) {
            clazz = findClass(className);
        }
    }
 
    return clazz;
}
```
以上是`ClassLoader.java`中`loadClass`方法的源码。动态加载内容只需要实现自己的`ClassLoader`就可以。  

Dalvik中也是通过Custom ClassLoader进行类加载的。  

## ClassLoader隔离
在JVM和Dalvik中，有没有可能同时存在两个包名和类名完全一样的类？答案是有可能。两者标记类的方式都是`ClassLoader id+PackageName+ClassName`，所以如果由不同`ClassLoader`进行加载，就有可能同时存在两个相同的类，并且此时一个`ClassLoader`的类实例无法强制转换成另一个`ClassLoader`中的类实例，`ClassLoader`起到了屏蔽隔离的作用。可以通过`instance.getClass().getClassLoader()`查看ClassLoader。   

那么在这种情况下如果我们通过不同的ClassLoader加载了三个文件`common.jar jar1.jar jar2.jar`，其中`jar1.jar`和`jar2.jar`都加载了`common.jar`，那么JVM上就会存在三份`common.jar`，由于上述隔离问题就可能造成类转换抛出异常。那么我们怎么避免这种情况呢？根本的手段就是JVM只加载一份`common.jar`，可以通过以下方式来做：  
- 让加载`jar1.jar`和`jar2.jar`的ClassLoader成为加载`common.jar`的ClassLoader的孩子，这样根据上面的加载过程在父亲中已经加载的`common.jar`就不会在孩子中再加载。  
- 重写`jar1`和`jar2`的ClassLoader，直接先去找加载`common`的ClassLoader看是否已经加载了`common`  
- 在生成`jar1`和`jar2`的时候去掉`common.jar`，这样就只有host中存有一份`common.jar`，再令加载它的`ClassLoader`成为加载jar1和jar2的`ClassLoader`的parent即可。  

其中第一、二种方式都无法应用到Dalvik虚拟机上，只有第三种方式可行。  

## Dalvik上的ClassLoader

本节主要参考[深入理解Android之Java虚拟机Dalvik]  
在介绍Dalvik的ClassLoader之前，先介绍一下Dalvik的字节码文件dex。早期Android手机的内存和存储比较小，需要对Class文件进行优化，比如很多Class文件可能都含有相同的字符串，这种就可以只放在一个地方而不需要每个Class文件都用一个常量池来存放。可以认为Dex文件是Class文件的一个变种，将不同的Class文件进行压缩优化后形成一个更小的但携带信息量不损失的Dex文件。  
![dex](images/blogs/dex.png)  

通过jadx等工具可以将dex文件还原为class文件。  

通过sdk build-tools下的dx工具可以将class文件压缩成dex文件(这个工具还可以dump出class文件内容，同目录下dexdump工具则可以dump出dex文件内容)。  

除了dex以外，其实还有odex文件，它是optimized dex的缩写，对dex文件针对系统具体平台和环境进行了优化，这种优化不是体积上的缩减，而是流程(比如类校验)上的提前操作，这样在载入这个odex的时候这些事情就不需要Dalvik在运行时去做，从而提高运行速度。odex文件所达到的优化效果需要依赖系统几个核心的模块(由`BOOTCLASSPATH`环境变量给出，一般是`/system/framework/`下的jar包，如`core.jar`)。在我看来它是一种静态分析，将dex中可以静态决定的内容结合运行环境去提前做好，比如将dex中一些类校验好并打上`VERIFIED`标签，将方法放在vtable虚表中访问。  

odex文件由dexopt工具生成，这个工具在SDK里面没有，但是看android源码的话可以在dalvik目录下看到dexopt模块，说明它可能跟Dalvik有关。事实上它是放在了Dalvik里面，由DalvikVM在加载一个dex文件的时候使用它生成odex文件，dalvik 实际上用的是odex文件。另外，其实在安装Apk的时候`PackageManagerService`会调用`installd`服务将apk中的`classes.dex`处理成odex，有些厂商也会提前优化，把系统镜像中Apk或者jar里面的class和dex提前生成odex，再删除原来的以减少体积。  

我们大致了解了dex和odex的格式和作用，下面来看`ClassLoader`。根据前面的分析，JVM上一个class运行的时候，同时存在的`ClassLoader`不止一个，而Dalvik采用了类似的机制，因此一个App运行的时候同时存在的`ClassLoader`也不止一个。在Android系统启动的时候需要加载核心类，即前面提到的`/system/framework/`中的jar包，所以它会启一个`ClassLoader`实例，它是`java.lang.BootClassLoader`类型，跟JVM上的`BootClassLoader`作用一样。App也会用到系统的类，所以App启动的时候会把这个`ClassLoader`实例传进去，另外App自己的类(保存在dex中)也需要一个`ClassLoader`来load，这就是`dalvik.system.PathClassLoader`，App启动的时候会创建一个它的实例，用来加载自己的dex，并且它的parent是`BootClassLoader`。我们可以通过以下代码验证：  

```
protected void onCreate(Bundle savedInstanceState){
     super.onCreate(savedInstanceState);
     ClassLoader loader = getClassLoader();
     while(loader != null){
         Log.i(TAG, "[onCreate] ClassLoader " + ":" + loader.toString());
         loader = loader.getParent();
     }
}
```

可见，一个运行中的App，至少有两个`ClassLoader`，其中`BootClassLoader`是整个系统所有`ClassLoader`实例的祖先，是`ClassLoader`树的根节点。我们要想加载自己的dex文件，就要实现自己的`ClassLoader`或者Hook掉App的`PathClassLoader`，前者相对较简单。如果希望实现自己的`ClassLoader`，加载一个新版本的dex文件里面的新类替换旧类，那就需要保证这个新dex比旧dex加载更早(因为双亲委托机制, 在首次加载成功后就会立即返回)，否则新dex将不会被加载。通过定义不同的ClassLoader(与旧dex的ClassLoader不具有父子关系)可以让新旧dex都加载，但这就会遇到前面说的隔离问题。  

前面说过，系统启动的时候就会加载`BootClassLoader`，它里面有个方法是调用Native的方法去加载Class，这个我认为我们App是无法去修改的(系统可以修改或者hook)，所以想做手脚只能在`PathClassLoader`或者`DexClassLoader`上做。两者都是Android中`BaseDexClassLoader`类的子类(在2.2源码上没有这个类，3.0以后的才有，所以你至少需要一套比较新的android源码才能跟本文一致, 本文基于4.4源码)，而`BaseDexClassLoader`又是抽象类`java.lang.ClassLoader`的子类，实现代码其实都在`BaseDexClassLoader`这个父类里面，只是把参数做了封装，因此导致`PathClassLoader`只能加载系统中已经安装过的apk，而`DexClassLoader`可以加载apk/jar/dex (后文说明原因)，包括从外存中加载未安装的apk。要了解dex等类是怎么加载的，就要打开`BaseDexClassLoader`源码。  

```
public BaseDexClassLoader(String dexPath, File optimizedDirectory,
    String libraryPath, ClassLoader parent) {
    super(parent);
    this.originalPath = dexPath;
    this.pathList = new DexPathList(this, dexPath, libraryPath, optimizedDirectory);
}
```

其他参数都还好理解，这个`optimizedDirectory`是什么鬼？构造函数中除了存下`dexPath`以外，还创建了一个`DexPathList`实例，传进去了一堆路径，这个又是个什么鬼？  

打开`DexPathList`瞄一眼(对了，忘记说，`dalvik`这几个`ClassLoader`虽然有源码不过位置在android源码项目的`/libcore/dalvik/src/main/java/dalvik/system`中哦，在`Sdk/platform/android-apilevel/android.jar`中只有class文件哦，反编译出来方法内容是空的哦，就是不给你看java文件哦，你只能反编译class文件看源码哦，科科)：  

```
final class DexPathList {
    private static final String DEX_SUFFIX = ".dex";
    private static final String JAR_SUFFIX = ".jar";
    private static final String ZIP_SUFFIX = ".zip";
    private static final String APK_SUFFIX = ".apk"; // 支持这四种文件
    ...
    public DexPathList(ClassLoader definingContext, String dexPath,
            String libraryPath, File optimizedDirectory) {
        // defensive codes
        ...
        
        this.definingContext = definingContext;
        this.dexElements = makeDexElements(splitDexPath(dexPath), optimizedDirectory, suppressedExceptions);
        this.nativeLibraryDirectories = splitLibraryPath(libraryPath);
    }
    
    private static Element[] makeDexElements(ArrayList<File> files, File optimizedDirectory, ArrayList<IOException> suppressedExceptions) {
       ArrayList<Element> elements = new ArrayList<Element>();
       /*
        * Open all files and load the (direct or contained) dex files
        * up front.
        */
       for (File file : files) {
           File zip = null;
           DexFile dex = null;
           String name = file.getName();

           if (name.endsWith(DEX_SUFFIX)) {
               // Raw dex file (not inside a zip/jar).
               try {
                   dex = loadDexFile(file, optimizedDirectory);
               }
           } else if (name.endsWith(APK_SUFFIX) || name.endsWith(JAR_SUFFIX)
                   || name.endsWith(ZIP_SUFFIX)) {
               zip = file;
               try {
                   dex = loadDexFile(file, optimizedDirectory);
               }
           } else if (file.isDirectory()) {
               // We support directories for looking up resources.
               // This is only useful for running libcore tests.
               elements.add(new Element(file, true, null, null));
           } else {
               System.logW("Unknown file type for: " + file);
           }

           if ((zip != null) || (dex != null)) {
               elements.add(new Element(file, false, zip, dex));
           }
       }

       return elements.toArray(new Element[elements.size()]);
   }
   
   private static DexFile loadDexFile(File file, File optimizedDirectory)
           throws IOException {
       if (optimizedDirectory == null) {
           return new DexFile(file);
       } else {
           String optimizedPath = optimizedPathFor(file, optimizedDirectory);
           return DexFile.loadDex(file.getPath(), optimizedPath, 0);
       }
   }
   
   private static String optimizedPathFor(File path,
          File optimizedDirectory) {
      String fileName = path.getName();
      if (!fileName.endsWith(DEX_SUFFIX)) {
          int lastDot = fileName.lastIndexOf(".");
          if (lastDot < 0) {
              fileName += DEX_SUFFIX;
          } else {
              StringBuilder sb = new StringBuilder(lastDot + 4);
              sb.append(fileName, 0, lastDot);
              sb.append(DEX_SUFFIX);
              fileName = sb.toString();
          }
      }

      File result = new File(optimizedDirectory, fileName);
      return result.getPath();
  }
```

代码较长，但是不难理解。先调用`makeDexElements`在路径里面找指定后缀的文件，找到了就添加到`elements`数组里面，最后将它复制出来返回给`dexElements`。在找的过程中对于找到的文件调用`loadDexFile`载入，在这个函数中又使用`optimizedDirectory`先查找是否存在优化路径，不存在就直接给new一个`DexFile`，存在的话就解析一下文件路径并调用`DexFile.loadDex`来载入。这个优化路径下的文件八九不离十就是我们前面说过的`odex`了。  

在`PathClassLoader`中调用父类构造方法的时候，`optimizedDirectory`这个参数传入的是`null`，也就是只允许在系统路径中去查找文件，所以就限制了搜索范围只能在已经安装了的Apk中。  

`DexPathList`中还有几个重要的方法，`findClass`，`findResource`，`findLibrary`，它们是实际被调用的类加载方法，`BaseDexClassLoader`重载了父类`ClassLoader`的`loadClass`方法，其实际就是调用了`DexPathList`中的`findClass`，而`DexPathList.findClass`中的实现代码也很简单，就是遍历之前创建的`DexFile`并调用`DexFile.loadClassBinaryName`来尝试加载类文件，最终调用到的是Native方法`defineClassNative`. 在4.0的源码中, `loadClassBinaryName`调用的`defineClass`方法已经是一个`native`方法了.  

至此，ClassLoader加载dex的过程已经完全清楚了。  

### 深入理解Dalvik类加载过程
上文讲到最终调用的是`defineClassNative`方法, 在其上的方法都是对Dex文件的封装, 解析则全都在`defineClassNative`中, 该方法返回一个Class对象. 这节我们就来看看, Dex文件是如何解析成Class对象的.  
虽然"深入理解XXX"都无可避免地要甩源码, 但对我来说C的源码真是阅读困难, 我们尽量少上源码多上解释.  

`private static native Class defineClassNative(String name, ClassLoader loader, int cookie)`是方法原型, 前两个参数都好理解, 文件名和所要装载入的`ClassLoader`, 单说第三个参数`cookie`, 这个参数由后文可知是一个指向`DexOrJar`的指针, 想必诸位也猜到了, 这其实代表的是要读的文件的类型.  
注意了, 这里是一个静态私有本地方法, 但是后面它的JNI实现的函数签名非常诡异, 见以下代码块. 小子才疏学浅, 还望各位dalao不吝指点.  

```
// /dalvik/vm/native/dalvik_system_DexFile.cpp
/*
 * 注释, 大致是说这是defineClassNative方法的实现, 从一个Dex文件载入Class, 跟一般VM的defineClass差不多. 它由虚拟机本身完成. 如果没找到类, 返回空指针, 如果其他问题则抛出异常.  
 */
static void Dalvik_dalvik_system_DexFile_defineClassNative(const u4* args,
    JValue* pResult) // 这就是我觉得非常诡异的地方: 参数列表. 没有ENV, 没有object, 有的只是两个指针.
{
    StringObject* nameObj = (StringObject*) args[0]; // 显然args这个指针是个u4数组, 每个成员代表一个地址, 它跟defineClassNative的参数列表一一对应.  
    Object* loader = (Object*) args[1];
    int cookie = args[2];
    ClassObject* clazz = NULL;
    DexOrJar* pDexOrJar = (DexOrJar*) cookie;
    DvmDex* pDvmDex;
    char* name;
    char* descriptor;

    name = dvmCreateCstrFromString(nameObj);
    descriptor = dvmDotToDescriptor(name);
    ALOGV("--- Explicit class load '%s' l=%p c=0x%08x",
        descriptor, loader, cookie);
    free(name);

    if (!validateCookie(cookie)) // 类型检查
        RETURN_VOID();

    // 分别调用不同的函数解析文件, 返回DvmDex*
    if (pDexOrJar->isDex)
        pDvmDex = dvmGetRawDexFileDex(pDexOrJar->pRawDexFile);
    else
        pDvmDex = dvmGetJarFileDex(pDexOrJar->pJarFile);

    /* once we load something, we can't unmap the storage */
    pDexOrJar->okayToFree = false;

    clazz = dvmDefineClass(pDvmDex, descriptor, loader); // 调用类定义函数, 解析内部表示的Dex
    Thread* self = dvmThreadSelf();
    if (dvmCheckException(self)) {
        /*注释. 检查是否发生了异常, 如果异常是没找到类, 则不管. 发生了异常则返回为空.
         */
        Object* excep = dvmGetException(self);
        if (strcmp(excep->clazz->descriptor,
                   "Ljava/lang/ClassNotFoundException;") == 0 ||
            strcmp(excep->clazz->descriptor,
                   "Ljava/lang/NoClassDefFoundError;") == 0)
        {
            dvmClearException(self);
        }
        clazz = NULL;
    }

    free(descriptor);
    RETURN_PTR(clazz);
}
```

上面最关键的两步就是`dvmGetRawDexFileDex(pDexOrJar->pRawDexFile)`和`dvmDefineClass(pDvmDex, descriptor, loader)`. 其中前者实际上就是一个`parser`, 基本上也没什么好看的, 只要了解一下Dex的内部表示`DvmDex`这个类(准确的说是结构体, 但反正它只是C上对类的表示, 所以我们这里就统称类了)即可, 这个类在`/dalvik/vm/DvmDex.h`中定义.  
所以重点就是后者了. 其定义在`/dalvik/vm/oo/Class.cpp`.  
`dvmDefineClass`方法直接调用了`findClassNoInit`方法, 顾名思义就是要"载入类, 不初始化". 这个方法相当长, 恐怕不贴代码讲不清楚, 贴全代码太占篇幅, 所以我就贴一下(我认为的)重点部分:  

```
static ClassObject* findClassNoInit(const char* descriptor, Object* loader,
    DvmDex* pDvmDex)
{
    //变量声明均不省略, 调试代码, 安全检测, 异常检查均省略
    Thread* self = dvmThreadSelf();
    ClassObject* clazz;     //最终返回的ClassObejct
    bool profilerNotified = false;

    //判断目标类是否有类加载器
    if (loader != NULL) {
        ...
    }

    if (dvmCheckException(self)) {
        ...// 如果一开始就有异常, 直接退出
    }
    clazz = dvmLookupClass(descriptor, loader, true); // 找找是否已经加载过了
    if (clazz == NULL) {
        const DexClassDef* pClassDef;

        dvmMethodTraceClassPrepBegin();
        profilerNotified = true;

        //判断是否存在DvmDex结构体对象，如果存在，则表示目标类为一个用户类，我们将要从一个解析的Dex文件中进行加载，对于一个解析过的Dex文件，则一定存在一个DvmDex结构对象的，所以pDvmDex一定不为空，若为空则表示目标类是一个系统类，虚拟机将调用searchBootPathForClass函数从启动路径下查找并加载类
        //我们自己加载Dex的话, 这里pDvmDex必不为空, 它代表上面完成解析的Dex文件
        if (pDvmDex == NULL) {
            pDvmDex = searchBootPathForClass(descriptor, &pClassDef);
        } else {
        //这里主函数调用dexFindClass并以DexFile数据结构的实例对象pDexFile作为参数，查找目标类定义资源，并将结果返回给pClassDef变量
            pClassDef = dexFindClass(pDvmDex->pDexFile, descriptor);
        }

        if (pDvmDex == NULL || pClassDef == NULL) {
            ...//什么都没找到, 出错了
            goto bail;
        }
        //获得了目标类所需的资源，调用loadClassFromDex函数对目标类进行加载
        clazz = loadClassFromDex(pDvmDex, pClassDef, loader);
        if (dvmCheckException(self)) {
            ...
            goto bail;
        }

        //链接类的过程中, 将类对象锁住，防止其他进程更改
        dvmLockObject(self, (Object*) clazz);
        clazz->initThreadId = self->threadId;

        /*
         * Add to hash table so lookups succeed.
         *
         * [Are circular references possible when linking a class?]
         * 以上是源码自带的注释哟, 你问我我问谁去. 理论上如果对类的定义不好的话, 是有可能发生循环引用的吧.
         */
        //将目标泪添加到hash表中
        if (!dvmAddClassToHash(clazz)) {
            /*
             * Another thread must have loaded the class after we
             * started but before we finished.  Discard what we've
             * done and leave some hints for the GC.
             *
             * (Yes, this happens.)
             * 以上也是原装注释2333 
             * 大致是说我们已经到达这个位置了但竟然有别的线程把这个类载入了(改了hash表), 于是我们出师未捷身先死
             * 末尾还郑重其事地说了句: 没错, 确实有可能发生这种事. 为了喜剧效果下面这句注释掉的代码我也不删了.
             */
            //ALOGW("WOW: somebody loaded %s simultaneously", descriptor);

            ...//释放锁和中间变量

            //从已加载类的系统哈希表中重新得到的类
            clazz = dvmLookupClass(descriptor, loader, true);
            assert(clazz != NULL);
            goto got_class;
        }
        dvmReleaseTrackedAlloc((Object*) clazz, NULL);

        //准备开始连接类
        if (!dvmLinkClass(clazz)) {

            //如果出错则将类清空
            removeClassFromHash(clazz);
            clazz->status = CLASS_ERROR;
            dvmFreeClassInnards(clazz);

            ...//通知等待的线程, 并释放对象锁, clazz置空
            ...//日志记录一下

            goto bail;
        }
        dvmObjectNotifyAll(self, (Object*) clazz);
        dvmUnlockObject(self, (Object*) clazz);

        //将类的状态添加到全局变量. 然鹅, 注释里面说"TODO: 这里或许应该原子操作"
        gDvm.numLoadedClasses++;
        gDvm.numDeclaredMethods +=
            clazz->virtualMethodCount + clazz->directMethodCount;
        gDvm.numDeclaredInstFields += clazz->ifieldCount;
        gDvm.numDeclaredStaticFields += clazz->sfieldCount;

        /*
         * 注释. 大意是这里缓存指向基本类(即java.lang.Object)的指针. 很多地方都会用到它, 最简单的方式就是在第一次用的时候来初始化这个指针, 而不是一开始就初始化.
         */
        if (gDvm.classJavaLangObject == NULL &&
            strcmp(descriptor, "Ljava/lang/Object;") == 0)
        {
            /* 只有bootclasspath loader可以走到这里, 也就是说loader参数应该为空.
             */
            assert(loader == NULL);
            gDvm.classJavaLangObject = clazz;
        }

    } else {
got_class:
        if (!dvmIsClassLinked(clazz) && clazz->status != CLASS_ERROR) {
            /* 注释. 类链接可能会发生竞争. 这里应该不会递归地调用, 如果有, 说明可能发生循环依赖了.
             * 一种情况除外: 我们在dvmLinkClass中强制载入了java.lang.Class类, 而它的父类是Object. 所以如果我们第一个载入的类恰好是Object类, 
             * 就会造成 Object->Class->Object这样的初始化顺序. 最简单的避免方式就是别最先载入Object, 所以我们是Foo->Class->Object这样的初始化.
             */
            dvmLockObject(self, (Object*) clazz);
            if (!dvmIsClassLinked(clazz) &&
                clazz->initThreadId == self->threadId)
            {
                ...// 循环链接了. 释放锁, 抛出异常, 走bail
                goto bail;
            }
            //如果检查发现clazz未链接成功但也没报错(这说明可能有别的线程在做链接), 就等待获取对象通知再进行检查, 直到成功或报错.
            while (!dvmIsClassLinked(clazz) && clazz->status != CLASS_ERROR) {
                dvmObjectWait(self, (Object*) clazz, 0, 0, false);
            }
            dvmUnlockObject(self, (Object*) clazz);
        }
        if (clazz->status == CLASS_ERROR) {
            ...//进入这里表明其他的载入过程记录了一次出错, 于是我们抛出异常并走bail 
            goto bail;
        }
    }

    ...//检查一些变量, 如clazz是否成功链接, 是否是Object类型, gDvm.classJavaLangClass是否不为空, 是否与clazz->clazz相等. 总之就是一些状态
    if (!dvmIsInterfaceClass(clazz)) {
        //如果不是接口, 还要检查虚方法计数是否等于虚表中的计数
        assert(clazz->vtableCount >= clazz->virtualMethodCount);
    }

bail:
    //错误处理
    if (profilerNotified)
        dvmMethodTraceClassPrepEnd();
    assert(clazz != NULL || dvmCheckException(self)); // 要么clazz不为空, 要么应该记录了有异常
    return clazz;
}
```

以上, 可以看到最核心的是四个方法`searchBootPathForClass` `dexFindClass` `loadClassFromDex`和`dvmLinkClass`, 前两个分别是对系统基本类和用户类的查找, 找的是Dex文件中对类的描述, 封装后返回, 传入的参数`descriptor`是形似"Landroid/debug/Stuff"的字符串. 根据这个返回的封装类, 我们可以很快在Dex文件中找到类数据. 第三个方法最后还会调用`loadClassFromDex0`, 两者都在`/dalvik/vm/oo/Class.cpp`文件中, 是将Dex文件实际解析为Class对象的方法. 最后一个则是"链接类", 所谓的链接就是"把不属于这个class直接定义但被它所使用的方法的实现入口找到", 在这个语境下就是链接类所实现的接口, 以及父类方法.  

这里我们主要看第三个方法`loadClassFromDex`. 上代码, 我把解析也放在里面方便就地理解:  

```
// 同样, 删除所有调试, 检查代码, 及部分注释
/*
 * 注释. 从指定的DEX文件载入指定的class. 相当于针对DexClassDef的高效 loadClass()+defineClass()
 * 失败时返回NULL, 如果遇到错误则还抛出异常.
 */
static ClassObject* loadClassFromDex(DvmDex* pDvmDex,
    const DexClassDef* pClassDef, Object* classLoader)
{
    ClassObject* result;
    DexClassDataHeader header;
    const u1* pEncodedData;
    const DexFile* pDexFile;
    ...
    pDexFile = pDvmDex->pDexFile;
    ...
    pEncodedData = dexGetClassData(pDexFile, pClassDef);// 获取编码后的数据

    if (pEncodedData != NULL) {
        dexReadClassDataHeader(&pEncodedData, &header); // 载入DexClassDataHeader
    } else {
        // Provide an all-zeroes header for the rest of the loading.
        memset(&header, 0, sizeof(header)); // 全0填充header
    }

    result = loadClassFromDex0(pDvmDex, pClassDef, &header, pEncodedData,
            classLoader);// 使用传入的参数+header+pEncodeData, 调用loadClassFromDex0
    ...
    return result;
}

/*
 * 注释. loadClassFromDex的帮手方法(Helper).
 */
static ClassObject* loadClassFromDex0(DvmDex* pDvmDex,
    const DexClassDef* pClassDef, const DexClassDataHeader* pHeader,
    const u1* pEncodedData, Object* classLoader)
{
    ClassObject* newClass = NULL;
    const DexFile* pDexFile;
    const char* descriptor;
    int i;

    pDexFile = pDvmDex->pDexFile;
    descriptor = dexGetClassDescriptor(pDexFile, pClassDef);

    /*
     * 注释. 保证访问标志中没有设置额外的标志位, 因为需要用标志位记录运行时状态.
     * 仅以下三种标志位被允许
     */
    const uint32_t EXPECTED_FILE_FLAGS = (ACC_CLASS_MASK | CLASS_ISPREVERIFIED |
                                          CLASS_ISOPTIMIZED);
    if ((pClassDef->accessFlags & ~EXPECTED_FILE_FLAGS) != 0) { // 其他标志位若不为0则返回NULL
        return NULL;
    }

    /*
     * 在GC堆上分配Class对象的存储空间, 让其他对象可以引用它.  
     * 绕过了通常的allocObject机制, 因为我们还不拥有所有的内存位.  
     * 假设java.lang.Class没有重写finalize()
     */
    if (classLoader == NULL &&
        strcmp(descriptor, "Ljava/lang/Class;") == 0) {//如果是系统在载入Class类, 则直接指向gDvm全局变量的classJavaLangClass
        newClass = gDvm.classJavaLangClass;
    } else {
        size_t size = classObjectSize(pHeader->staticFieldsSize);//获取类对象大小
        newClass = (ClassObject*) dvmMalloc(size, ALLOC_NON_MOVING);//分配内存空间
    }
    if (newClass == NULL)//分配失败, 返回NULL
        return NULL;

    // 下面几句直接看名字就行了, 对类对象实例初始化
    DVM_OBJECT_INIT(newClass, gDvm.classJavaLangClass);
    dvmSetClassSerialNumber(newClass);
    newClass->descriptor = descriptor;
    SET_CLASS_FLAG(newClass, pClassDef->accessFlags);
    dvmSetFieldObject((Object *)newClass,
                      OFFSETOF_MEMBER(ClassObject, classLoader),
                      (Object *)classLoader);
    newClass->pDvmDex = pDvmDex;
    newClass->primitiveType = PRIM_NOT;
    newClass->status = CLASS_IDX;

    /*
     * 注释. 先给对象的指针域里搞个代表超类索引的占位符, 等链接器来把它替换成一个
     * 真正的指向ClassObject的指针. 我没有搞个单独的superclassIdx的域, 这样就可以每个类节省几个字节.
     * 由于newClass->super既不会被dvmFreeClassInnards访问也不会被它释放, 所以这样做是安全的
     * 这里设置的是超类指针.
     */
    assert(sizeof(u4) == sizeof(ClassObject*)); /* 32-bit check */
    newClass->super = (ClassObject*) pClassDef->superclassIdx;

    /*
     * 把类引用的索引放入指针区域.
     * 由于newClass->interfaces既不会被dvmFreeClassInnards访问也不会被它释放, 所以这样做是GC安全的
     * 这里设置的是接口指针.
     */
    const DexTypeList* pInterfacesList;
    pInterfacesList = dexGetInterfacesList(pDexFile, pClassDef);
    if (pInterfacesList != NULL) {
        newClass->interfaceCount = pInterfacesList->size;
        newClass->interfaces = (ClassObject**) dvmLinearAlloc(classLoader,
                newClass->interfaceCount * sizeof(ClassObject*));

        for (i = 0; i < newClass->interfaceCount; i++) {
            const DexTypeItem* pType = dexGetTypeItem(pInterfacesList, i);
            newClass->interfaces[i] = (ClassObject*)(u4) pType->typeIdx;
        }
        dvmLinearReadOnly(classLoader, newClass->interfaces);
    }

    /* load field definitions */

    /*
     * 注释. 预分配足够空间的类对象, 将静态域添加到尾部. 这些空间的大小固定且在分配时已知.
     * 这可能增加zygote共享空间. 堆整理时如果需要移动ClassObject对象则需要小心, 因为内部用的是域指针进行传递.
     * 但至少到此处时这些域指针还在对象堆上.
     */
    // 加载静态域(静态字段)
    if (pHeader->staticFieldsSize != 0) {
        int count = (int) pHeader->staticFieldsSize;
        u4 lastIndex = 0;
        DexField field;

        newClass->sfieldCount = count;
        for (i = 0; i < count; i++) {
            dexReadClassDataField(&pEncodedData, &field, &lastIndex);
            loadSFieldFromDex(newClass, &field, &newClass->sfields[i]);
        }
    }
    // 加载实例字段
    if (pHeader->instanceFieldsSize != 0) {
        int count = (int) pHeader->instanceFieldsSize;
        u4 lastIndex = 0;
        DexField field;

        newClass->ifieldCount = count;
        newClass->ifields = (InstField*) dvmLinearAlloc(classLoader,
                count * sizeof(InstField));
        for (i = 0; i < count; i++) {
            dexReadClassDataField(&pEncodedData, &field, &lastIndex);
            loadIFieldFromDex(newClass, &field, &newClass->ifields[i]);
        }
        dvmLinearReadOnly(classLoader, newClass->ifields);
    }

    /*
     * 注释. 加载方法定义. 分为两批, 直接方法和虚方法.
     * 如果这个类的注册表已经生成了, 且开启了precise GC, 我们获取指向这些注册表的指针. 
     * 我们知道它们流式转为DEX文件时顺序跟这些方法在DEX中出现的顺序相同.
     * 如果类没有被预验证(pre-verified), 那在类初始化过程中类会被验证, 并且生成这些注册表.
     */
    u4 classDefIdx = dexGetIndexForClassDef(pDexFile, pClassDef);
    const void* classMapData;
    u4 numMethods;

    if (gDvm.preciseGc) {
        classMapData =
            dvmRegisterMapGetClassData(pDexFile, classDefIdx, &numMethods);

        /* sanity check */
        if (classMapData != NULL &&
            pHeader->directMethodsSize + pHeader->virtualMethodsSize != numMethods)
        {
            .../* abandon */
        }
    } else {
        classMapData = NULL;
    }
    // 加载类方法
    if (pHeader->directMethodsSize != 0) {
        int count = (int) pHeader->directMethodsSize;
        u4 lastIndex = 0;
        DexMethod method;
        // 为方法分配空间, 然后挨个加载方法
        newClass->directMethodCount = count;
        newClass->directMethods = (Method*) dvmLinearAlloc(classLoader,
                count * sizeof(Method));
        for (i = 0; i < count; i++) {
            dexReadClassDataMethod(&pEncodedData, &method, &lastIndex);
            loadMethodFromDex(newClass, &method, &newClass->directMethods[i]);
            if (classMapData != NULL) {
                const RegisterMap* pMap = dvmRegisterMapGetNext(&classMapData);
                if (dvmRegisterMapGetFormat(pMap) != kRegMapFormatNone) {
                    newClass->directMethods[i].registerMap = pMap;
                    /* TODO: add rigorous checks */
                    assert((newClass->directMethods[i].registersSize+7) / 8 ==
                        newClass->directMethods[i].registerMap->regWidth);
                }
            }
        }
        dvmLinearReadOnly(classLoader, newClass->directMethods);
    }
    // 加载虚方法
    if (pHeader->virtualMethodsSize != 0) {
        int count = (int) pHeader->virtualMethodsSize;
        u4 lastIndex = 0;
        DexMethod method;
        //过程跟加载类方法差不多
        newClass->virtualMethodCount = count;
        newClass->virtualMethods = (Method*) dvmLinearAlloc(classLoader,
                count * sizeof(Method));
        for (i = 0; i < count; i++) {
            dexReadClassDataMethod(&pEncodedData, &method, &lastIndex);
            loadMethodFromDex(newClass, &method, &newClass->virtualMethods[i]);
            if (classMapData != NULL) {
                const RegisterMap* pMap = dvmRegisterMapGetNext(&classMapData);
                if (dvmRegisterMapGetFormat(pMap) != kRegMapFormatNone) {
                    newClass->virtualMethods[i].registerMap = pMap;
                    /* TODO: add rigorous checks */
                    assert((newClass->virtualMethods[i].registersSize+7) / 8 ==
                        newClass->virtualMethods[i].registerMap->regWidth);
                }
            }
        }
        dvmLinearReadOnly(classLoader, newClass->virtualMethods);
    }

    newClass->sourceFile = dexGetSourceFile(pDexFile, pClassDef);

    /* caller must call dvmReleaseTrackedAlloc */
    return newClass;
}
```

更细的`loadMethodFromDex`和`loadIFieldFromDex`我就不再深入了, 看名字也知道是从Dex中载入Method和Field. 以上方法大致做了以下事情:  
- 申请内存空间, 用于存储Class对象  
- 设置超类指针和接口指针  
- 加载静态域和实例字段  
- 加载类方法, 包括直接方法和虚方法  

最后返回类对象ClassObject的指针.  

关于`dvnLinkClass`链接类, 就不再进行分析了, 目前我们已经知道了足够的类加载过程信息. 有兴趣的同学可以继续深入看, Android是怎么通过ClassObject解析出真正实现并完成链接的. 需要指出的是, 这里会检查和加载父类方法, 所以如果父类的类对象没有被初始化, 就会在此时加载和初始化了.  

是时候再来一张图了:  
![dex_class_load](images/blogs/dex_class_load.png)

### 自定义ClassLoader

本节主要参考[Custom Class Loading in Dalvik]  
以上的分析后我们知道了，如果想要加载任意位置的任意dex文件，我们需要实现自己的`DexClassLoader`，甚至我们可以模仿`DexClassLoader`或者`BaseDexClassLoader`去实现一个自己的继承`ClassLoader`的类，我们也可以重载`loadClass`方法实现自己的载入类的逻辑。这种Hack方式甚至允许我们不采用双亲代理的方式去查找类，但缺点是这样做App的稳定性会降低。  

如此下来，对于一般的类我们已经可以加载新dex替换旧dex，但是还面临一个问题，就是如果想要替换四大组件的话怎么办？我们知道，在Android中，四大组件都必须要在Manifest中注册过才能使用，如果新加载一个Activity，它没有被注册过，也无法使用。另外还有个问题，就是资源的问题，新加载的类中的资源id和已有的资源实例根本对不上。  

以上两个问题可以说是一个问题，就是上下文问题。动态加载框架要解决的核心问题也是这个问题。为此我也研究了Android上动态加载的相关技术，参见另一篇文章。  

## Dalvik上Dex字节码的执行
载入了dex文件之后如何执行，其实大体上来说跟普通程序的执行过程并无二致，这里介绍这个大体流程。dalvik对字节码的处理程序在源码目录的`dalvik/vm/mterp/out`下，每个平台包含一个汇编文件和一个c文件.  
```
./
├── InterpAsm-allstubs.S
├── InterpAsm-armv4t.S
├── InterpAsm-armv5te.S
├── InterpAsm-armv5te-vfp.S
├── InterpAsm-armv7-a-neon.S
├── InterpAsm-armv7-a.S
├── InterpAsm-x86-atom.S
├── InterpAsm-x86.S
├── InterpC-allstubs.c
├── InterpC-armv4t.c
├── InterpC-armv5te.c
├── InterpC-armv5te-vfp.c
├── InterpC-armv7-a.c
├── InterpC-armv7-a-neon.c
├── InterpC-portdbg.c
├── InterpC-portstd.c
├── InterpC-x86-atom.c
└── InterpC-x86.c
```

打开任意平台的汇编和c, 就可以知道, 其实这些程序就是解释执行Dex字节码,它们把你的Dex指令(比如`new`), 翻译成对应平台CPU可以理解的汇编指令. 非常的不神秘.  

## Dalvik的启动

## Dalvik的垃圾回收机制
众所周知JVM(以Hotspot为例)规定了垃圾回收机制, 实现中就会面临垃圾回收算法的选择, 在`full-gc`时会引起`stop-the-world`现象, 那么基于JVM的Dalvik虚拟机又是怎么管理垃圾回收的呢?  


## 参考
感谢以下文章及其作者.  

- [深入理解Android之Java虚拟机Dalvik]  
- [dex format]  
- [Android动态加载基础 ClassLoader工作机制]  
- [Custom Class Loading in Dalvik]  
- [how-to-load-a-java-class-dynamically-on-android-dalvik]  

[深入理解Android之Java虚拟机Dalvik]: http://blog.csdn.net/innost/article/details/50377905  
[dex format]: https://source.android.com/devices/tech/dalvik/dex-format.html  
[Android动态加载基础 ClassLoader工作机制]: https://segmentfault.com/a/1190000004062880  
[Custom Class Loading in Dalvik]: http://android-developers.blogspot.com/2011/07/custom-class-loading-in-dalvik.html  
[how-to-load-a-java-class-dynamically-on-android-dalvik]: http://stackoverflow.com/questions/3022454/how-to-load-a-java-class-dynamically-on-android-dalvik  
