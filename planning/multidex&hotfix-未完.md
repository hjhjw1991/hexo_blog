# 插件化/多dex/热修复/动态加载技术学习
[携程插件化实践]: http://www.infoq.com/cn/articles/ctrip-android-dynamic-loading  
[Android插件化开发: 动态加载Apk]: http://www.jianshu.com/p/01a91a9b7a19  
[Android插件化: 动态升级]: http://www.trinea.cn/android/android-plugin/  
[Android插件化原理解析——概要]: http://weishu.me/2016/01/28/understand-plugin-framework-overview/  
[Android插件化: 从入门到放弃]: http://www.infoq.com/cn/articles/android-plug-ins-from-entry-to-give-up  
[Android 插件化的 过去 现在 未来]: http://kymjs.com/code/2016/05/04/01  
[Android动态加载技术 系列索引]: https://segmentfault.com/a/1190000004086213  
[Android动态加载入门 简单加载模式]: https://segmentfault.com/a/1190000004062952  
[Android动态加载进阶 代理Activity模式]: https://segmentfault.com/a/1190000004062972  
[Android动态加载黑科技 动态创建Activity模式]: https://segmentfault.com/a/1190000004077469  

[携程插件化框架]: https://github.com/CtripMobile/DynamicAPK  
[携程Android App插件化和动态加载实践]: http://www.infoq.com/cn/articles/ctrip-android-dynamic-loading  
[蘑菇街组件与插件化-插件篇]: http://mogu.io/117-117  
[基于Atlas的插件化框架]: https://github.com/limpoxe/Android-Plugin-Framework  
[Xposed Framework详解]: http://blog.csdn.net/innost/article/details/50461783  

>插件化技术，简单说就是将功能以插件的形式提供给App，从而实现免重安装就可以启用新功能的效果，其实现原理主要是利用Dalvik虚拟机DexClassLoader对dex、jar、apk文件的载入机制。利用这个原理，可以实现多dex支持、功能更新、增量更新、加载第三方App以及在线热修复等效果

先讲一点历史。早在2012年就有人写了AndroidDynamicLoader框架，实现从任意App中加载任意Fragment到自己的App中来的效果，可以动态加载不同的Fragment，这就是插件化的雏形。之后经过一系列的发展，包括dynamic-load-apk、DroidPlugin等开荒项目，直到2015年底，Github上已经出现了很多插件化框架。  

既然可以动态加载Activity，加载资源，甚至加载Apk，就有人想到我们是不是可以通过动态加载的方式，服务器通知客户端去下载一个什么文件然后重启自己(甚至不需要重启)，就可以达到升级App或者修复bug的效果？如果可以这样，那想必在线紧急修复会变得更简单些，而App升级也再不需要重新下载安装了。这时候就出现了热修复技术。先是在iOS上出现的JSPatch热修复，通过Runtime机制可以线上修复任何一个类的任何一个方法。后来Android这边2015年9月就有人找到了方法，就是Andfix。  

讲完了历史，再来说基础。要学习插件化技术，有些基础知识是必备的，不知道或者一知半解的赶紧去补课。首先是Binder机制，Binder作为一个代理，起到了连接客户端和服务端的关键作用，在Android系统中很多事情都离不开Binder，四大组件没了Binder寸步难行。Binder和服务端的代码作为App我们没办法修改，我们可以做的就是在客户端这边动手脚。  

其次是Apk的打包过程。App项目从源码和资源到Apk文件，先后经历了资源打包、dex生成、签名等过程，其中最重要的就是资源打包，即AAPT这一步。如果宿主和插件的资源id出现冲突，一种解决方式就是在这里做修改。  

第三，Apk在手机上的安装过程。手机安装Apk的有时候会遇到下载异常，资源包不能解析，什么原因？你需要知道安装Apk的这段代码在什么地方，Apk下载到本地后放在了什么位置，会生成什么文件。如果要实现增量更新，下载的增量包放在什么地方，命名规则是什么。  

第四，是App的启动流程。我们都知道点击手机上App图标可以通过系统Launcher机制启动App，在App内部可以通过startActivity启动Activity，但是他们启动的原理是什么？main函数在什么地方？我们对main函数所在的类做修改，就可以实现插件化。  

第五，插件需要对dex和资源进行加载。插件可能是apk也可能是so，不会生成R.id，直接加载dex的话是没法使用插件自带的资源的。插件化框架自然已经是处理了这个问题，我们需要自己想想怎么解决这个问题，让资源可以正确加载。  

第六，多个插件协同工作。不同的插件会不会相互影响？如何避免？  

## 实现插件化的三种思路
目前实现插件化有三种思路：静态代理，动态替换，Dex合并。  

静态代理，就是任玉刚DynamicLoadApk的思路。写一个PluginActivity，继承自Activity，把涉及生命周期的方法全部重写，让插件的Activity继承这个PluginActivity，再用个ActivityManager来管理多个插件。  

动态替换，就是在不同层次进行Hook。可以在Activity中Hook getAsset，使用自己的AssetManager和ResourceManager；可以更高一层，重写startActivity，修改ActivityThread、Instrumentation；还可以更高，去修改AMS、PMS。层次越高越抽象，相对来说就越自由越灵活。  

Dex合并，这是热修复采用的思想，其原理是利用ClassLoader对dex文件加载的顺序，达到修复包dex的方法覆盖旧dex的同名方法的目的。在Android中，原生Apk自带的dex是通过Dalvik的PathClassLoader来加载的，而插件dex是通过DexClassLoader来加载，在Dalvik上，如果加载了两个具有相同命名空间的dex，那么哪个先加载，哪个就起作用，其后的同名方法全都不起作用。利用这个原理，可以优先加载修复包dex中的方法，从而达到覆盖旧方法实现热修复的目的。  

<!-- 我去补充ClassLoader和Dalvik知识去了，等补充完毕了回来写剩下的部分，即具体怎么实现插件化 -->
<!-- 我补充知识回来了，也看了Dalvik中ClassLoader的源码，感觉差不多了，然后我尝试了一下动态加载一个dex文件，失败。。。等我成功后再来更新 -->
