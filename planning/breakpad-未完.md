# Breakpad崩溃信息收集工具

Breakpad是Google Chromium项目开源的一个用于崩溃收集的工具, 源码见[这里][source]. 由于工作需要, 对它进行了调研, 这里记录一下体会.  

## 原理

## 用法
首先是编译breakpad_client客户端静态库.  
针对Android, breakpad源码项目提供了一个说明文档和一个示例代码.  
下面开始列出坑:
1. github上的项目有坑, 少一个lss目录.  
用以下命令 `git clone https://chromium.googlesource.com/linux-syscall-support $(BREAKPAD_PATH)/src/third_party/lss`补上.  
2. 目前breakpad支持的abi还只有有限几种, 特别是, 不支持64位的arm64-v8a架构. 如果有这个需求可能就需要寻找别的工具了. 另外, 虽然声称支持armeabi, 但是默认是用armeabi-v7a编译(你会看到Android.mk里写的LOCAL_ARM_MODE是arm), 所以如果你引用它的项目配了其他的abi, 就会因为指令集不同的缘故, 报汇编错误.    
3. Android编译的时候会发现缺少了一堆依赖, 原因是breakpad的git项目里确实就没有这几个依赖, 而且补上还会相当麻烦和臃肿, 大致看了都是单元测试和模拟测试的依赖, 于是统统删掉了.  


## 局限

## 参考



[source]: https://chromium.googlesource.com/breakpad/breakpad  

