# RxJava初探

RxJava是一个开源库, 它用于在 Java VM 上编写异步的、基于事件的程序，它使用观察者模式使程序代码的逻辑更清晰.  

>技术世界真是日新月异，学会的技术还没有机会使用，就已经过时了。学而时习之，温故而知新。  

开坑的时候RxJava才出1.0, 等我现在想填坑了已经出到了2.1, 甚至3 pre都出了, 好在RxJava的提交还是非常频繁的, 说明其活力未减, 仍然有大量开发者在为之做贡献.  

## Reactive Programming
在讲RxJava之前, 我们先来看看Reactive Programming的概念, 因为RxJava的诞生意义就在于让Reactive Programming更简单.  
中文译作响应式编程. 所谓响应式编程, 本质上是事件驱动的编程模型, 是"生产者-消费者"关系. 在视频[overview of reactive programming]中(需自备梯子)Netflix的专家Jafar解释了什么是响应式编程以及Netflix为什么采用这种方式, 他用SQL来比喻响应式编程: 每一个SQL查询都可以跟其他SQL查询复合在一起组成一个新的SQL查询然后从数据库中获取需要的数据, 类似的每一个Observable都可以跟其他的Observable任意组合成新的Observable然后被Observer处理. 他指出, 所有能以迭代方式处理的事件都可以运用流(Stream)来响应式处理, 而响应式处理的宗旨在于更快地提供处理结果, 从而达到不需要MessageQueue和额外缓存空间的目的.  

## RxJava
RxJava, 全称`Reactive Extensions for the JVM`, Netflix发起并开源, . 

[wiki-Reactive programming]: https://en.wikipedia.org/wiki/Reactive_programming  
[wiki-Functional reactive programming]: https://en.wikipedia.org/wiki/Functional_reactive_programming  
[overview of reactive programming]: https://hackhands.com/overview-of-reactive-programming/  
[给 Android 开发者的 RxJava 详解]: http://gank.io/post/560e15be2dca930e00da1083#toc_1 "给 Android 开发者的 RxJava 详解"  
[frponandroid]: http://slides.com/yaroslavheriatovych/frponandroid#/ "Functional Reactive Programming on Android"  
[深入浅出RxJava（一：基础篇）]: http://blog.csdn.net/lzyzsd/article/details/41833541/  
[深入浅出RxJava(二：操作符)]: http://blog.csdn.net/lzyzsd/article/details/44094895  
[深入浅出RxJava三--响应式的好处]: http://blog.csdn.net/lzyzsd/article/details/44891933  
[深入浅出RxJava四-在Android中使用响应式编程]: http://blog.csdn.net/lzyzsd/article/details/45033611  
[RxJava & RxAndroid备忘]: http://www.jcodecraeer.com/a/anzhuokaifa/androidkaifa/2015/0701/3125.html  
[RxJava初探]: http://www.jcodecraeer.com/a/anzhuokaifa/androidkaifa/2015/0511/2863.html  
[RxJava原理介绍]: http://blog.csdn.net/fjczhong2008/article/details/29648207  
[关于RxJava最友好的文章]: https://juejin.im/post/580103f20e3dd90057fc3e6d  
[关于RxJava最友好的文章（进阶）]: https://juejin.im/post/5818777f67f356005871ef2c  
