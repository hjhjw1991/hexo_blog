# Android模块化之aar  

>类比jar, aar其实就是Android平台的jar, 由Android Library项目导出, 包含资源文件，因此有了aar之后可以不必将整个项目作为依赖导入.   


[Android模块化编程之引用本地的AAR]: http://stormzhang.com/android/2015/03/01/android-reference-local-aar/  
[Reference a local .aar in your Android project]: http://kevinpelgrims.com/blog/2014/05/18/reference-a-local-aar-in-your-android-project/  
