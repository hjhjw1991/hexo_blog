# 重看WindowManagerService

>今天看"Android艺术探索"的时候, 感觉WindowManagerService讲得过于简单了, 根据我的经验, Android项目中的Service一般都相当复杂, 于是我产生了疑惑, 开始看源码并诞生本文.  

本文依旧基于Android 4.0的源码, 为什么没有用最新的源码呢, 是因为我认为版本间的变迁是可以通过提交历史和更新记录来追溯的, 而我们对一件事情一无所知的时候, 最好还是追本溯源了解一下它本来的面貌, 这样一来我们可以知道它诞生的使命, 二来也可以在心中绘制一份蓝图, 对比它后来的变化, 可以一定程度上提升自身心法水平, 三来如果有适配问题我们也可以更快速地定位.  
以上纯属个人看法, 在对Android所有架构有了较清晰认识后我也可能会优先看最新的代码及提交记录.  

## 回顾Service的结构
WMS作为系统Service的一个, 它的实例化肯定也是在系统初始化的时候(AMS中), 用一个Key保存下来的, 结构应该也是代理或桥接, 于是我们知道至少有三个类要看: Service本身, Service实现, Service管理的对象.  
通过代码搜索WindowManager我们找到了`WindowManagerService` `WindowManagerGlobal` `WindowManagerImpl`这三个类. 通过实际使用我们知道获取到的都是`WindowManager`这个接口的实现类的对象, 所以`WindowManager`接口我们可以跳过了.  
接下来我们通过系统启动过程去找`Context.WINDOW_SERVICE`的添加, 从而顺藤摸瓜找到`WindowManager`实现类的定义.  

## 寻找WindowManager实现类



[Android解析WindowManagerService（一）WMS的诞生]: https://blog.csdn.net/itachi85/article/details/78186741  
[深入理解WindowManagerService 节选]: https://blog.csdn.net/innost/article/details/47660193  
[Git袁博客文章]: https://www.kancloud.cn/alex_wsc/androidsystem/403620  

