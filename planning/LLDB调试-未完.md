# LLDB调试技术

[iOS 开发者旅途中的指南针 - LLDB 调试技术]: http://swiftcafe.io/2015/09/05/lldb-debug/  
[与调试器共舞 - LLDB 的华尔兹]: https://objccn.io/issue-19-2/  
[iOS调试-LLDB学习总结]: http://www.jianshu.com/p/d6a0a5e39b0e  
[LLDB调试器使用简介]: http://southpeak.github.io/2015/01/25/tool-lldb/  
[熟练使用 LLDB，让你调试事半功倍]: http://ios.jobbole.com/83393/  
[LLDB调试命令初探]: http://www.starfelix.com/blog/2014/03/17/lldbdiao-shi-ming-ling-chu-tan/  

[lldb-gdb]: http://lldb.llvm.org/lldb-gdb.html  


>使用Android Studio开发Android的同学, 如果必须要学会至少一种命令行调试native代码的方式的话, 建议学习LLDB.  
