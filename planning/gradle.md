# Gradle探秘
>Gradle出现之初，就是为了替代老旧的Ant编译工具，它自定义了一套基于Groovy的DSL用来编译解释脚本，跟Ant和Maven相比优势在于更加灵活，更加适合喜欢diy的程序员们。

[深入理解Android:Gradle]: http://blog.csdn.net/innost/article/details/48228651  
[Groovy Documentation]: http://www.groovy-lang.org/documentation.html  
[Groovy API]: http://www.groovy-lang.org/api.html  
[Groovy基础——Closure(闭包)详解]: http://attis-wong-163-com.iteye.com/blog/1239819  
[Gradle学习系列之三——读懂Gradle语法]: http://www.tuicool.com/articles/eyaQni  
[Groovy 及其闭包简介(以及和gradle的关系)]: http://ju.outofmemory.cn/entry/218231  
[Gradle DSL]: https://docs.gradle.org/current/dsl/  
[Gradle API(for plugin developers)]: https://docs.gradle.org/current/javadoc/  


## 构建之痛
混沌之初，程序员们还都在用编译器一遍一遍手工编译链接运行他们的项目。随着世界的发展，项目越来越庞大也越来越复杂，而编译链接运行却往往是重复枯燥又耗时的工作，程序员们就想，这种重复劳动怎么能占据我那么多时间？看起来不复杂，只需要少量的配置就可以自动完成，那为什么不开发个工具来做这件事呢？我们可是程序员啊！  

于是出现了Build系统，出现了make工具，只需要写好.mk文件，make就可以帮你完成所有你能想到的枯燥乏味的构建任务。Android系统就是在Build系统下编译的。  

然而，make还是不够灵活，每次有什么修改都需要去写.mk文件，有没有可能我只需要写少数一些配置，就可以自动触发相应的构建任务呢？然后我们有了Ant，有了Maven。Ant随着Eclipse的流行而流行起来，Maven工具也广受欢迎，但是个人来讲我是很不喜欢Maven用xml的方式来写配置文件的，这种方式实在太麻烦了，而且非常不灵活，我想要动态判断运行条件来执行不同的任务都不行。特别是移动项目，我想要针对不同的渠道，去打包不同的源文件，或者我想要有一个针对移动项目早就预定义好了多种常规任务的工具，默认情况下我什么都不需要定制就可以应付大多数场景。  

所以，才有了Gradle。  

## Gradle  
上面扯了这些，最后出现了一个踩着七彩祥云的英雄Gradle，那么Gradle是什么？它能做什么？  

还是从需求来说。我们希望构建工具是这样的：1，它要足够简单，简单到上手只需要很少一点的学习成本，最好不需要成本；2，它要足够灵活，想做什么就能做什么，最好跟编程似的；3，最好，它已经把我要做的给做成了标准流程，我只需要给它我的项目，给它少量几个参数。所以，Gradle选择了Groovy来实现：Groovy是JVM上Java的一种方言，函数式的语法和对Java的支持令其上手成本极低(事实上，Groovy内部将Groovy文件编译为class文件，交给虚拟机执行)
；它是一种编程语言，所以可以定义自己的函数、变量、类和方法，也可以引入别人定义好的插件，能做什么只受限于你的编程能力；Gradle使用Groovy编写好了一套常用的规则，在移动开发中你能想到的大多数任务和概念，Gradle都已经做了定义，你可以直接拿来用。  

我们很多人可能写过不少`.gradle`的配置文件，但是对Gradle构建工具却未必有多了解。下面就来认识一下这个犀利的工具。

### Groovy  
工欲善其事必先利其器，Gradle既然是依托于Groovy之上的，那么我们就必须要先来认识一下Groovy，否则别说DSL方言了，你连人家用的词汇都听不懂，你还怎么跟人交流。  
Groovy自己说它是JVM上的Python，其实这有点"Clojure说自己是JVM上的Haskell"的意思，它无非就是一个翻译官，它把类似于Python的语法风格的语言，翻译成JVM听得懂的字节码。Java编译器将Java源代码翻译成Java字节码，而Groovy编译器将Groovy源代码翻译成Java字节码。对于JVM来说，都是字节码，所以并无区别。  

介绍到这里，更多的只需要去熟悉Groovy的具体语法知识和内建结构，如果你有Python基础，学起来应该很快，很多语法都一样的，就不说了。这里只提一下，Groovy中把基本数据类型和一些常用的数据结构都对应到了Java中的包转类型和常用数据结构上，比如list就对应Java的List接口，底层常用ArrayList实现。  

另外，Groovy里面有一个非常重要的类叫做闭包(Closure)，类似于Python和Java的lambda表达式，跟Javascript的闭包也有相似，在定义的时候需要用花括号包起来，除非显式指定没有参数(`{ -> closure_body}`)否则默认有一个参数叫做`it`(`{"Hello $it"}` 完全等同于 `{it -> "Hello $it"}`)，跟Java的this和Python的self类似。在作为函数参数的时候，如果参数列表最后一个是闭包，则调用函数的时候参数列表可以不加小括号。闭包的调用方式有两种，`closure_name.call(params)`和`closure_name(params)`。更多有关闭包的语法请参考[官方文档][Groovy Documentation]。  

### 读懂Gradle  
Gradle是一种声明式的构建工具。在执行时，Gradle并不会一开始便顺序执行build.gradle文件中的内容，而是分为两个阶段，第一个阶段是配置阶段，然后才是实际的执行阶段。在配置阶段，Gradle将读取所有`build.gradle`文件的所有内容来配置Project和Task等，比如设置Project和Task的Property，处理Task之间的依赖关系等。  

让我们先来看看以下非常简单的Task：  
```
task showDescription1 << {
   description = 'this is task showDescription'
   println description
}


task showDescription2 << {
   println description
}
showDescription2.description = 'this is task showDescription'


task showDescription3 << {
   println description
}

showDescription3 {
   description = 'this is task showDescription'
}
```
以上三个Task完成的功能都相同，即先设置Task的`description`属性，然后输出到命令行。他们的不同在于对`description`属性的设置方式。对于`showDescription1`它在定义这个Task的同时就设置了description属性；对于`showDescription2`它是作为project的一个property被访问的，通过访问它来定义它的属性description；对于`showDescription3`它是作为project的一个方法被访问的，这个方法接受一个Closure作为参数，方法内的内容是在闭包上的delegate对象上执行的(默认情况下，delegate指向parent，这里parent就是`showDescription3`这个Task)。  

在Gradle中，对于每一个Task，Gradle会在Project中创建一个同名的Property和一个同名的方法，所以我们既可以像访问Property那样访问Task，也可以像调用方法那样调用Task。  

Groovy会对所有字段自动生成getter和setter(对于字段`xxyyzz`，自动生成`getXxyyzz`和`setXxyyzz`方法)，在调用的时候这些字段就像public的一样可以写成直接访问的方式(`binding.variable`形式)，这使得可读性有提高，在其内部实际上还是调用的setter和getter方法。在Groovy中如果用`def`来定义了字段，则默认它是完全私有的，不能通过这样的方式访问。另外，如果在定义变量的时候用了`def`或者指定了类型，则这个变量的作用域只在当前层，即便是当前层定义的函数中也无法访问。以上都与Groovy处理源码文件的具体方式有关，Groovy到底是怎么处理源码的，可以`groovyc`编译之后看class文件。  

要读懂Gradle，必须要理解Groovy中闭包的delegate机制。我们可能会经常在Gradle中看到这样的代码：  
```
task hello{
    doLast{
        println "Hello World!"
    }
}
```
这就是利用了闭包的delegate机制来增强了可读性。下面我们借助一段代码来理解delegate机制：  
```
class Child{
    private String name
}

class Parent{
    Child child = new Child()
    
    void configChild(Closure closure){
        closure.delegate = child // #1
        closure.setResolveStrategy Closure.DELEGATE_FIRST // #2
        closure() // #3
    }
}

def parent = new Parent()
parent.configChild{ // #4
    name = "child name" // #5
}

print parent.child.name // #6
```
这段代码，光看#4~#6，就是跟Task相似的形式，我们知道是在设置name属性的值并输出，但是，我们没有在`configChild`方法中指明name属性属于Child，从调用语句我们可能以为name是parent的属性，而实际上它又确实是完成了对Child.name属性的设置。事实上，玄机就在`configChild`方法体中。在默认情况下，#5的name确实会被认为是parent的属性，但是我们在configChild方法中传入了一个闭包，并且将闭包的delegate设置为了child，把它的解析策略设置为`DELEGATE_FIRST`(默认情况下`setResolveStrategy`的值是`Closure.OWNER_FIRST`，即先查找闭包的owner，这里是parent，如果owner存在则在owner上执行闭包代码)，这样闭包就会先去查找`delegate`并尝试在`delegate`上执行闭包代码。在这个例子中，#4对方法用省略括号的方式调用，传入的作为参数的闭包代码是#5，#3执行闭包代码。需要注意的是，方法定义中接受哪种闭包，调用的时候就必须传入这种类型的闭包，比如这里方法体中对闭包是伪无参数调用，如果传入一个有两个参数的闭包是会报错的。查[Gradle API][Gradle API(for plugin developers)]文档可以知道Gradle SDK中闭包方法都接受什么样的定义。  

上面showDescription3的例子就是用到了delegate这个特性，只不过作为一个Task它的流程更多一步，在showDescription3()方法内部首先调用了showDescription3.configure()方法，在这个方法中执行闭包代码。  

在build.gradle中我们调用方法的时候没有像#4那样指明方法所属的对象`parent`，而是直接`task()`、`apply()`，这是因为在不指明调用对象的情况下，Gradle自动将调用对象设置为当前Project，所以上述调用其实和`project.task()`、`project.apply()`等价。`configuration()`、`dependencies()`等方法同理。查看[Gradle文档][Gradle DSL]可以了解Gradle DSL都定义了哪些方法。  

举个例子，对于`configurations()`方法，该方法实际上会将闭包参数的delegate设置成`ConfigurationContainer`，然后在该`ConfigurationContainer`上执行闭包中的代码。再比如，`dependencies()`方法，该方法会将所跟闭包的`delegate`设置成`DependencyHandler`。  

还有，Project还定义了`configure(Object object,Closure configureClosure)`方法，该方法是专门用来配置对象的(比如Task)，它会将`configureClosure`的`delegate`设置成`object`，之后`configureClosure`中的执行代码其实是在`object`上执行的。和`Groovy Bean`一样，`delegate`机制的一个好处是可以增加所创建DSL的可读性。  

回过头来再看上面`task hello`这个例子。本质上来说一个Project的build文件，就是一系列Task的集合，Project用`TaskContainer`接口的create方法创建一个Task，其中一个create方法的声明是这样的：`Task create(String name, Closure configureClosure)`，使用给定的选项和闭包生成一个Task实例。事实上`task`关键字可以看作这些create方法的别名。`configureClosure`将会传入到Task的构造函数中，`delegate`对象就是当前Task对象，所以在`configureClosure`中可以使用Task中所有方法，包括`doLast`。于是这个例子可以被等价改写为如下形式：  
```
hello = TaskContainer.create("hello"，{doLast{println "Hello World!"}})
```
这里面有两个闭包，一个是传给doLast的闭包，一个是传给create的闭包。  

Task有哪些方法可以调用，都接受什么样的闭包，这些信息可以查Gradle官方文档得到。  

### 在Gradle中使用SDK
前面说到了Gradle中项目是一系列Task的定义，其实在官方DSL文档中，Gradle有三种脚本，即settings script、build script以及Gradle自身的init script，前两者在项目中都可以看到。这些脚本实际上各自代理了一个特定的类，比如build script代理的就是Project类，因此我们才可以在脚本中直接使用一些方法和属性。像`allprojects`、`buildscripts`等其实都是这些类里面预先定义好的接受闭包为参数的方法。  

既然脚本本质上是一些类和方法定义，那么理论上我们也可以自行定义其他脚本并import进来，或者使用Gradle/Groovy/Java的SDK来制定自己的脚本。下面就尝试在`build.gradle`中引用Gradle API中的`org.gradle.api.logging`来记录build日志。  

可以直接通过logging.log的方式记录日志，查看Project文档发现提供了getLogger方法获取当前Project的Logger对象，它是实现了SLF4J Logger接口的一个类，于是可以在build.gradle中任意位置加入以下代码进行记录：
```
getLogger().lifecycle(msg)
```
在Gradle构建中就可以将msg记录下来。log一共有六种level，不同的Level可能有不同的表现，默认只有lifecycle会在终端打印出信息，它也是log的默认level。全部六种level从高到低排列如下：  

|Level	|Used for|
|:------|------------------|
|ERROR	|Error messages|
|QUIET	|Important information messages|
|WARNING	|Warning messages|
|LIFECYCLE	|Progress information messages|
|INFO	|Information messages|
|DEBUG	|Debug messages|

在命令行下log参数和等级对应如下  

|Option	|Outputs Log Levels|
|:------|------------------|
|no logging options	|LIFECYCLE and higher|
|-q or --quiet	|QUIET and higher|
|-i or --info	|INFO and higher|
|-d or --debug	|DEBUG and higher (that is, all log messages)|

这样就实现了在build脚本中记录和输出日志的功能。Project中提供了`LoggerManager`允许你将logger的输出进行重定向。Gradle还集成了Java Util Logging, Jakarta Commons Logging 和 Log4j 工具，他们的输出会被重定向到Gradle的日志系统中。另外，也可以使用自己定义的Logger代替Gradle默认的Logger。更详细的用法见[Gradle Userguide - Logging](https://docs.gradle.org/current/userguide/logging.html)。  

在Gradle中，可以像Java一样通过`import`关键字导入其他的类和文件，包括Java和Groovy类。  

0.9以后可以用`apply from:"path/to/your_gradle.gradle"`来导入其他gradle文件，但这个文件不能与当前文件有冲突(比如，不能apply同名插件)。  

可以自己编写插件来使用，就像android插件一样，只需要实现`Plugin<Project>`接口即可，Project的apply方法会调用Plugin接口的apply方法，并传入当前Project作为参数，在Plugin的apply中通常将需要使用的插件加入到project.extensions(`project.extensions.add("property_name", YourPluginClass)`)中，从而可以以属性的方式访问这个插件。关于插件的定义和使用见[custom plugins](https://docs.gradle.org/current/userguide/userguide_single.html#custom_plugins)。  

可以导入Ant的build文件，用`ant.importBuild 'build_file.xml'`即可，详见文档[use ant](https://docs.gradle.org/current/userguide/ant.html)。  

在`build.gradle`文件中定义的公共属性，可以在子项目中的`build.gradle`继承使用，这也是在顶部定义的`build.gradle`和`setting.gradle`可以作用到子项目的原因。  

有关如何组织多个`build.gradle`的更多信息见[Gradle organizing logic](https://docs.gradle.org/current/userguide/organizing_build_logic.html)  

经过测试，Gradle也可以直接import和使用Java/Groovy的类，甚至可以引用自己定义的源文件。Groovy限于随Gradle分发的版本，目前还不清楚它与standalone的Groovy有什么不同。  

## Gradle 自定义Task
根据[这个git项目](https://github.com/vincentbrison/vb-android-app-quality.git)的配置, 自己定义了一个用来检查代码风格的插件, 其中定义了checkstyle/pmd/findbugs三个插件的任务和他们的输出目录.  
根据前面的介绍, task关键字是一个接受闭包作为参数的创建Task的create方法的别名, 其实通过IDE查看可以发现task是Project接口类定义的一个接口函数, 返回一个Task实例, 在gradle wrapper的抽象类AbstractProject有默认实现, 是调用`this.taskContainer.create()`接口来实际创建. 但是这里, 我们定义的task是如下形式:  
```
task checkstyle(type: Checkstyle) {
    configFile file("$configDir/checkstyle/checkstyle.xml")
    configProperties.checkstyleSuppressionsPath = file("$configDir/checkstyle/suppressions.xml").absolutePath
    source 'src'
    include '**/*.java'
    exclude '**/gen/**'
    classpath = files()
}
```
`checkstyle`这个task name后面跟了参数, 还指定了type, 这是什么意思呢? 显然, 它不再单纯只是作为name了, 查看`checkstyle`的定义, 发现它实现的是`create(String var1, Class<T> var2)`这个接口, 其中`T`是`Task`的子类. 有意思, `type`后面这个名字正好就是`Checkstyle`这样一个继承了`SourceTask`的类的类名. 看来它指定的是参数`type`的值为`Checkstyle`. 奇怪, 这个接口只说了第二个参数是个`Task`子类的类型, 并没有说第二个参数叫做`type`. 查看该接口的继承关系, 发现gradle wrapper有默认实现, 是这个样子的:`public <T extends Task> T create(String name, Class<T> type)`, 真相大白. 诶, 等等, 闭包呢? 怎么没有闭包什么事了? 那后面的内容怎么作用的? 这里就来到我觉得很有意思的地方了, 以下内容纯属猜测没有验证: 在创建`checkstyle`这个Task的时候, 真实的调用关系是这样的, 首先将`checkstyle, type: Checkstyle`作为`TaskContainer.create(String var1, Class<T> var2)`的两个参数, 创建了一个`Task`, 然后给这个`Task`传递了一个`Closure`作为它的`doFirst`的参数, 最后把这个构造好的`Task`传递给`Project.task(String name)`作为参数(此时这个Task只传递了它的name), 存进任务Map里. 所以后面这种`findbugs(type: FindBugs, dependsOn: "assembleDebug")`的声明方式, 就是在生成了Task之后给它的`dependsOn`函数传了个参数`"assembleDebug"`, 再后面的闭包就是传给它的`doFirst`了.  
以上这些内容都是我瞎猜的. 总之按照上面这个gradle文件配置好四大质量检查工具之后, 需要在模块根目录的`build.gradle`里面添加一句`apply from: 'path/to/your/gradle.gradle'`应用到你想使用的项目上, 然后在`gradle sync`之后就可以通过`gradle taskname`直接执行这里面的task了, 在IDE的`gradle`视图中也能看到自己定义的这几个任务.  
显然, 如果想要复用这个gradle文件, 只需要把它放在一个公共访问的目录下, 然后在需要的项目中`apply`就可以了.  

