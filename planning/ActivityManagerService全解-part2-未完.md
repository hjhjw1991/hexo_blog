#【Android教程】ActivityManagerService全解(2/5)

又叫【花样劝退教程】。本文讲ActivityManagerService系统服务-由于这个东西涉及的面实在广, 前面我又没有写过其他的相关内容, 所以这个话题我分5个部分来介绍.

[hjhjw1991]: https://hjhjw1991.github.io  
[android Application Component研究之Activity(一)]: http://blog.csdn.net/windskier/article/details/7096521  
[深入理解 Android 卷II:第6章 深入理解ActivityManagerService]: http://wiki.jikexueyuan.com/project/deep-android-v2/activity.html  
[图解Activity的启动过程]: http://blog.csdn.net/TellH/article/details/57418651  

>笔者呕心沥血跟高冷的AMS交流了数个回合, 以笔者卧病在床数月为代价, 终于难产出上一篇文章, 如今笔者稍有恢复, 咱们继续来采访AMS家族.  

采访者: [hjhjw1991]  
被采访者: `ActivityManagerService`家族  
地点: `Android 4.0`源码目录  
时间: 2017-07-31  

## ActivityManagerNative, ActivityManagerProxy与ActivityManager

