
## 9 Patch Resizable Bitmap
[Create Resizable Bitmaps (9-Patch files)]: https://developer.android.com/studio/write/draw9patch.html  
[9-patch Android 可拉伸背景图]: https://www.zybuluo.com/dengzhirong/note/131544  
[9patch作为不失真背景]: http://blog.csdn.net/ouyang_peng/article/details/9242889  

>9 patch图又有人叫九宫格图，或九格图。它的出现是为了满足图片拉伸时想要保持某些部分不失真不变形的需要。  

背景图自适应失真问题  
开发时候往往需要自适应容器大小、内容大小、屏幕大小和横竖屏的背景图，比如列表的背景图，随着列表高度变化背景图应该也要跟随变化，比如标题栏的背景图，无论横屏竖屏、分辨率高或低，都要能自动填充满，而且不失真。  
一般是采用先切图后拼凑的做法，但Android针对这种情况专门制作了一种.9.png格式。  

.9.png格式是标准的png格式，只是在最外面一圈额外增加了1px的边框，用来定义图片中可扩展的区域和静态不变的区域。left和top边框中交叉的部分是可拉伸的，未选中的部分是静态不变的，这个可以标记多个拉伸区域。right和bottom边框中交叉的部分是内容部分，比如文字内容等，这个参数是可选的，只能标记一个内容区域，未定义的话有可能导致内容无法显示。两种组合都会把图片分成9个patch。.9.png图片一般保存在res/drawable下，引用的时候跟普通png图片一样只用写名字。从apk中解压出的.9图片已经去掉了周围那一圈像素，使用的时候必须加上不然AS会报错。  

Android SDK的tools下提供了制作.9.png的工具，draw9patch.jar，通过他可以把普通png图片编辑为.9.png。  

