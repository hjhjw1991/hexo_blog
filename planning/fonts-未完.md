# Android中的中文字体
[Unicode中文和特殊字符的编码范围 - 基于unicode 5.0](http://www.cnblogs.com/sosoft/p/3456631.html)  
unicode在线查询: https://unicode-table.com/en/#kangxi-radicals  

最初Android使用的是DroidSansFallback.ttf这个TrueTypeFont字体, 是Google自己搞出来的.  
从Android 6.0开始, 使用Roboto-fonts家族替代了DroidSans家族成为默认字体, 但在Android 7.0为止还保留着DroidSansFallback.ttf这个字体, 在Android 7.1则已经没有了这个字体.  

http://www.unicode.org/versions/Unicode9.0.0/ch18.pdf  这是unicode对CJK编码范围的定义  
http://blog.csdn.net/flyeek/article/details/44057999  Android下字体工作原理  
在系统的`ZygoteInit()`时会通过`Class.forName()` 载入`android.graphics.TypeFace`类, 从而导致执行它的静态方法块, 它的静态方法块在Android 5.0以后首先会读取系统的字体配置, 然后载入默认的系统字体. 字体配置在文件`/system/etc/fonts.xml`. 在Android 4.0~4.2, native层负责加载系统字体配置文件和备用字体配置文件.  
native层主要是skia引擎的android移植版.  
在4.x版本中, 默认的中文字体对应DroidSansFallback.ttf字体文件, 这个字体文件包含了除中文外的很多其它字符.  
在5.x版本中, 默认的中文字体对应NotoSansHans-Regular.otf字体文件, 这是Adobe与Google共同开发的中文字体, 属于Google的Noto字体系列, Adobe则称为思源字体.  
在7.1中, 默认是NotoSansCJK-Regular.ttc. 解析后有8种字体, 最高支持到扩展E的u2ce93, 扩展E支持108个字符. 基本CJK支持20941个.  
