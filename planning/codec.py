# -*- coding:utf-8 -*-
"""Codec. convert any encoded string to utf-8. requires python 3.5+
"""

import chardet

# supported encodings
class ENCODE:
    UTF_8 = r"utf-8"
    @staticmethod
    def valid(enc):
        return ENCODE.UTF_8 == enc.lower()

# decorator to encode parameters, Chinese for example
def encode(enc):
    def decorator(method):
        def wrapper(*args, **kwds):
            if ENCODE.valid(enc):
                newargs = []
                for arg in args:
                    new_arg = arg
                    if isinstance(arg, str):
                        new_arg = arg.encode(enc)
                    newargs.append(new_arg)
                args = newargs
            return method(*args, **kwds)
        return wrapper
    return decorator
        
