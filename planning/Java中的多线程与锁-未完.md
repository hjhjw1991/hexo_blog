# Java中的多线程与锁

欢迎来到并发编程的世界, 本文将会涉及但不限于以下类和概念: Sync, NonfairSync, FairSync, ReentrantLock, AbstractQueuedSynchronizer, CAS, 自旋锁, 偏向锁, 锁升级.  

说实话我下笔写这篇文章的时候压力是山大的, 怕学艺不精误人子弟, 但是看到如此能够解决实际问题的设计我又忍不住想写一写, 写的过程更多是帮助自己理解吧, 写得不对望各位不吝指教. 我估计在数个月之后我可能会就并发编程问题再写博客讨论.  

顺便一提, 写本文的时候我已经过了一遍<Java并发编程的艺术>以及<深入理解Java虚拟机>, 应该是有帮助到我理解这一块内容的.  

## 临界区是兵家必争之地--锁的诞生
并发程序如果要访问临界区域, 就必然需要通过某种机制避免读写和写写冲突, 锁是其中常见的一种. 锁, 顾名思义, 就是将临界区域锁上, 不允许上锁的人以外的人访问.  
先来说说临界区域. 以Java为例, 对象实例其实都是虚拟机上一段内存数据, 但是对象和对象之间不一定是独立的, 他们可能会访问公共的方法或者变量, 比如类静态方法, 类静态变量, 那么对他们来说, 这些他们可以同时访问的内存数据就是临界区域. 同样的, 对于同一个对象, 它在内存中是同一块数据, 所以在不同的线程访问者来看, 它的方法和成员都是临界区域.  
于是, 如果有可能多个线程去同时访问同一个对象, 就可能面临脏数据问题: 一个线程修改了值, 但另一个线程并没有及时得知, 它取了旧值. 这个时候整个对象状态在这两个线程中就不一致了, 这可能导致A线程以为对象已经更新了, 而B线程以为没有更新, 从而引发一系列问题. 更糟糕的是, 在并发环境下, 如果没有确定的调度策略(例如JVM的`happen-before`规则), 读写的顺序是不一定的.  
为了使多个线程的视角下同一个临界区域的状态一致, 就有必要将并发访问转化为顺序访问, 尽管访问顺序可能无法保证. 锁的机制就是为了这个目的诞生. 在访问者持有临界区域的锁时, 其他未持有锁的访问者无法对临界区域进行访问.  
"读-写"排列组合的操作顺序对数据有几种可能的影响, 其中"读-读"是不需要加锁的, 其他过程都需要加锁.  

## 如何争夺临界区--锁的调度机制
一种有用且常用的机制产生之后, 自然有针对这种机制的各种研究诞生. 锁的最大问题就是, 效率不高, 本来并发编程就是想提升程序指令吞吐量, 但是在访问临界区域的时候所有访问者都会被堵着, 只有获得锁的那一个可以访问. 这还可以接收, 因为这是锁机制的天然限制, 但是锁的最大问题不在锁上了之后, 而在获取锁的过程, 也就是锁调度机制, 跟线程调度类似, 如果锁调度不好, 访问者可能会饿死, 甚至可能发生死锁.  
目前死锁问题还没有比较好的方法解决, 因为它基本上算是编程时考虑不周而引入的问题, 而且事先进行排查也不太容易, 比如有可能是因为对使用的库或框架背后的原理理解不深, 导致不当地使用了这些库, 使得它们与你自己的代码之间发生了死锁, 这种情况你只排查自己的代码或者只排查库里面的代码都无法发现问题.  

本节重点讨论各种锁调度机制, 当然这里有个前提, 锁本身作为一个抽象概念, 与访问者和被访问的临界区域都无关. 实际操作中可能由于我们自身的限制, 使得在使用锁的时候不小心违反了这个原则, 例如对于同一段同步代码, 用了一个跟线程有关的对象作为锁, 这样子的话线程A获得的锁跟线程B获得的锁并不是同一个, 导致锁失效, 当然IDE已经能够帮我们检查这种静态代码的编写错误, 会提醒你用于加锁的锁对象是一个临时对象, 而不是不可变对象, 但是这种检查也不完全.  
我们从问题出发来引入各种机制.  

### 直觉方案--先到先得, 公平锁
在最原始的情况下, 两个线程争夺一个锁的时候, 必然会有一个先来后到的执行顺序, 最直觉的解决方案就是, 先到的先获得锁, 后到的就等待前一个拥有者释放锁, 锁依次传下去.  
这就是公平锁的概念. 所有请求同一个锁的线程排成队列, 只有最先进入队列的线程能获得锁, 换句话说, 就是等待时间最长的线程能获取到锁.  
名字里的公平是以线程的角度来看的, 作为更晚到达的线程, 更晚获得锁, 这一点看似是毋庸置疑的"公平分配". 这倒是不同于我一开始的以为, 我的理解是当争夺锁的时候, "所有线程以同等概率获得锁"才是"公平". Anyway, 我们只要知道现在大家认同的都是前面那种解释就行了.  
公平锁是一个实现很简单的模型, 应用范围比较广.  

### 公平锁不公平--特权阶级, 非公平锁
公平锁在实际应用中有两个大问题, 导致它的公平对于线程来说其实不公平: 1. 如果前序任务的线程持有锁的时间很长(比如, 它获得锁之后由于线程调度原因进入了挂起状态, 在这个过程中它仍然是持有锁的), 则后续任务无法获取锁, 即便后续任务只需要"用一下下"就能完成工作; 2. 有些任务可能具有高优先级, 导致它不完成任务的话依赖它的一大坨重要逻辑就无法继续, 但是在公平锁模型下它只能等前面的线程释放锁之后才能获得锁.  

因此, 就产生了这样一种锁调度策略, 让等待的线程们不以固定的到达顺序来获得锁, 这就是所谓非公平锁. 我认为, 凡是满足这个条件的锁都可以叫做非公平锁, 但网上搜索这个关键词, 多数时候是我们下面讲的第一种锁.  

#### 第一种: 贪心锁
具体机制就是, 当一个线程尝试获得锁的时候, 它首先进行获取锁的操作, 万一真得到了, 就很可能继续执行, 否则就乖乖进入到等待队列, 按顺序等待被分配锁. 其实这种锁我不知道它的名字, 我只知道Java中是以这种机制实现的非公平锁. 由于它一开始就尝试获取锁, 所以我给它取名叫"贪心锁".  
在多核情况下, 贪心锁比公平锁的效率更高, 这是因为公平锁模型需要维护一个等待队列, 这种维护在多核环境意味着线程切换的消耗较大, 而且无论任何线程进来都要加入到队列中. 贪心锁虽然也维护这样一个队列, 但是它一旦一开始就成功获得锁, 那这个队列就完全不必变动, 直到锁被释放, 这中间是没有线程上下文切换的.  

#### 第二种: 偏向锁
上文已经提到了, 获得锁的时候线程上下文切换是一个很大的影响效率的因素, 因此围绕减少切换次数而展开的研究就顺理成章.  
对实际编程模型进行统计, 发现一个线程使用过锁之后, 它再次请求同一个锁的概率是很高的, 而多线程环境下发生锁争夺的概率并不高, 于是就发展出了偏向锁这种方式.  
偏向锁, 简单说就是, 锁里面记录一下上一次是谁拥有的锁, 下一次分配锁的时候, 如果锁发现请求者就是上一次拥有锁的线程, 就直接分配给它, 也就是"偏向于"它. 这样, 在发生这种事情的时候当前线程能够更快获得锁, 不需要比较锁标记位, 也避免线程上下文切换.  
通常来说, 偏向锁基于"大多数时候不存在对同一个锁的竞争"这种经验假设, 当实际发生竞争的时候, 偏向锁模式应该宣告失败, 而采用更严格的锁模式来保证数据正确性.  

### 非公平锁的代价
非公平锁虽然在整体上提升了执行效率, 让花费在线程上下文切换的时间减少, 但它是以等待队列的等待时间延长作为代价的, 因此对线程来说其实没有一个执行保证.  
除此之外, 非公平锁跟公平锁一样, 有可能存在同一个线程持有锁的时间过长, 同时它也并没有解决上个小节开头提出的两个问题.  

### 锁的获取过程--自旋锁与可重入锁
这里的两个概念都针对的是锁的获取过程, 而非锁的分配策略.  

自旋锁具有这样的特征: 一个线程在尝试获得锁失败后, 并不立即放弃, 而是继续运行一段时间, 在此期间再次尝试获取锁. 继续运行的这个过程就称为自旋(Spin).  
这种情形非常常见, 例如其他线程持有锁之后其实很快就会释放锁, 但本线程一开始尝试的时候暂时还没有释放. 显然自旋锁可以减少这种情况的线程上下文切换, 而且很适合应用在贪心锁上.  

可重入锁具有这样的特征: 一个线程在尝试获得锁成功后, 可以再次获得同一把锁. 再次获得的过程就叫做重入(Reenter), 具有这种特征的锁就是可重入锁(ReentrantLock).  
在解释可重入锁的出现原因之前, 先来看不具有这种机制的时候, 公平锁和非公平锁在遇到同一个线程请求同一把锁的时候会怎么做.  
线程A在执行中获取了锁L, 并在执行过程中再次尝试获取锁L, 再次尝试的时候发现, L被获取了, 于是在此处线程A被放入了等待队列(公平锁)或者原地自旋(非公平锁加自旋), 无论哪种, 都导致线程A无法继续执行, 产生死锁.  
线程多次获取同一把锁的情形是很常见的, 比如递归调用, 又比如使用了同一把锁守护不同的调用函数(Java中的synchronized方法).  
因此, 在实际实现中, 对开发人员开放的锁必须要允许可重入, 只有在保证不可能发生递归使用同一把锁或者同一把锁守护可能被同时调用的不同过程这两种情况时, 使用非可重入的锁才是安全的.  

## Java中锁的实现
本小节涉及以下几个类或接口的源码: ReentrantLock, Lock, FairSync, NonfairSync, Sync, AbstractQueuedSynchronizer.  

我们通过看源码, 来认识Java中这几个类都是什么锁模型.  
先看一个类结构图:  
![lock in java](/images/blogs/lock_in_java.jpg)  

以上揭示了Java中这几个类的关系, 其中Unsafe由于它不暴露给开发者, 所以我们暂时不看. 有兴趣的同学可以等待我另一篇难产的分析Unsafe包的文章.  

### ReentrantLock, Sync

`ReentrantLock`是我们可以直接拿来用的一个类, 可能也是我们接触最多的(如果不是完全没接触过的话)一种锁. 它的关键代码如下:  
```
// in java/util/concurrent/locks/ReentrantLock.class
public class ReentrantLock implements Lock, Serializable {
    public ReentrantLock() {
        this.sync = new ReentrantLock.NonfairSync(); // 默认实例化非公平锁
    }

    public ReentrantLock(boolean var1) {
        this.sync = (ReentrantLock.Sync)(var1?new ReentrantLock.FairSync():new ReentrantLock.NonfairSync()); // 可选择实例化公平锁还是非公平锁
    }
    
    public void lock() {
        this.sync.lock();
    }
    public boolean tryLock() {
        return this.sync.nonfairTryAcquire(1);
    }
    public void unlock() {
        this.sync.release(1);
    }
}
```
多看几个方法就会发现它是个代理类, 它代理的是它的内部类对象`Sync sync`, 所有方法都是转交给`sync`来执行.  
来看`Sync`类的代码:  
```
abstract static class Sync extends AbstractQueuedSynchronizer {
    abstract void lock();

    final boolean nonfairTryAcquire(int var1) { // 非公平获取锁的默认实现, 参数表示要获取锁的次数
        Thread var2 = Thread.currentThread();
        int var3 = this.getState();
        if(var3 == 0) { // 当前没有人持有锁, 立即尝试获取锁
            if(this.compareAndSetState(0, var1)) {
                this.setExclusiveOwnerThread(var2);
                return true;
            }
        } else if(var2 == this.getExclusiveOwnerThread()) { // 当前线程已经持有锁
            int var4 = var3 + var1; // 锁计数增加
            if(var4 < 0) {
                throw new Error("Maximum lock count exceeded");
            }

            this.setState(var4); // 更新锁计数状态
            return true;
        }

        return false; // 获取锁失败
    }

    protected final boolean tryRelease(int var1) { // 释放锁的默认实现, 参数表示释放锁的次数, 返回值表示锁是否被完全释放
        int var2 = this.getState() - var1;
        if(Thread.currentThread() != this.getExclusiveOwnerThread()) { // 当前线程并不持有锁
            throw new IllegalMonitorStateException();
        } else {
            boolean var3 = false;
            if(var2 == 0) { // 锁计数已经归零, 则将锁的持有者清空
                var3 = true;
                this.setExclusiveOwnerThread((Thread)null);
            }

            this.setState(var2); // 更新锁计数状态
            return var3;
        }
    }

    protected final boolean isHeldExclusively() {
        return this.getExclusiveOwnerThread() == Thread.currentThread(); // 判断当前线程是否独占锁
    }

    final ConditionObject newCondition() {
        return new ConditionObject(this); // 构造新的条件对象. 调用者获得锁之后还会检查是否满足该条件对象描述的条件. 一般用于描述调用者需要的其他资源.
    }
}
```

从代码可以看出, 它至少实现了非公平锁和可重入锁两个模型. 我们同时也注意到它是个抽象类, 只有`lock()`这一个抽象方法(对, 你没看错, 它没有`unlock`方法), 它的直接父类是`AbstractQueuedSynchronizer`. 稍后我们来看这个父类, 现在先看它的子类.  
`Sync`这个内部类有两个子类, `FairSync`和`NonfairSync`, 看名字你也猜到是啥了, 我就只贴他们的`lock()`实现.  
```
static final class FairSync extends ReentrantLock.Sync {
    
    final void lock() {
        this.acquire(1);
        // 直接调用了acquire方法, 而这个方法是在AbstractQueuedSynchronizer中实现的
        // 具体就是它先调用了tryAcquire(), 失败的话再调用acquireQueued()入队, 入队成功就调用selfInterrupt()
    }

    // 由于acquire会调用tryAcquire因此我把这个方法也贴出来了
    protected final boolean tryAcquire(int var1) {
        Thread var2 = Thread.currentThread();
        int var3 = this.getState();
        if(var3 == 0) { // 如果当前锁不被任何线程持有, 且队列中没有更优先的请求者, 则立即尝试持有锁
            if(!this.hasQueuedPredecessors() && this.compareAndSetState(0, var1)) {
                this.setExclusiveOwnerThread(var2);
                return true;
            }
        } else if(var2 == this.getExclusiveOwnerThread()) { // 否则如果锁被当前线程持有, 就增加锁计数
            int var4 = var3 + var1;
            if(var4 < 0) {
                throw new Error("Maximum lock count exceeded");
            }

            this.setState(var4);
            return true;
        }

        return false; // 获取锁失败
    }
}

static final class NonfairSync extends ReentrantLock.Sync {

    final void lock() {
        if(this.compareAndSetState(0, 1)) { // 立即尝试获取锁, 失败的话调用父类acquire
            this.setExclusiveOwnerThread(Thread.currentThread());
        } else {
            this.acquire(1);
        }

    }

    protected final boolean tryAcquire(int var1) {
        return this.nonfairTryAcquire(var1); // 调用nonfairTryAcquire尝试获取锁
    }
}
```

说是只贴`lock()`实现, 但实际上已经贴出了全部代码. 以上代码我想我也不必解释什么了, 我的注释已经足够.  
有趣的是, 在非公平锁的实现中, 调用`lock()`的时候立即尝试了一次获取锁, 失败的情况下通过调用`acquire()`又尝试了一次, 所以其实在非公平锁的这个实现中一开始获取锁的操作尝试了两次, 非要说的话可以看做一瞬间的自旋.  

### AbstractQueuedSynchronizer--Java中锁的基石
在说`AbstractQueuedSynchronizer`之前, 先来看看它的作者.  
这个类的作者名叫Doug Lea, 他曾经是JCP委员, 现在是ACM会员和OpenJDK管理委员会成员. 他有两本著名的书, 一本叫<Concurrent Programming in Java: Design Principles and Patterns>, 另一本叫<Java Concurrency in Practice>, 中文版分别叫<Java并发编程: 设计原则与模式>和<Java并发编程实战>. 他是并发编程方面的专家.  
不仅如此, 他还写过一个库叫做`dlmalloc`, 明显这是个C库, GNU的C库glibc中, `ptmalloc`就是基于它实现的. `dlmalloc`的意思是`Doug Lea's Malloc`.  
所以说Java程序员最好还是会点C/C++, 不然源码都看不懂.  

源码中这个类提供了一份比较详尽的注释, 用来说明它的模型和使用要点. 我先将这份注释简单理解一下, 然后我们再来看源码.  
>本类提供一个框架, 用来实现依赖于FIFO等待队列的阻塞锁和有关的同步量(例如信号量semaphore, 事件event). 本类适合大多数用单一原子整数表示状态的同步量. 子类需要定义那些会改变状态或状态含义的方法, 这些方法都被本类定义为`protected`. 除此之外, 其他方法完成了队列和阻塞相关的机制. 子类可以维护其他的状态, 但只有`getState()` `setState()` `compareAndSetState`这三个方法所维护的那个原子更新的整数会被用来做同步.  
子类应该被定义为非public的内部帮手类, 用来实现跟它相关联的类的同步属性. AbstractQueuedSynchronizer类本身不实现任何同步相关的接口, 它只定义了类似 acquireInterruptibly 这样的方法, 在具体的lock或同步量的实现中可以调用这些方法.  
这个类支持独占模式(Exclusive)和共享模式(Shared), 两者可以同时存在. 在独占模式中, 一旦被一个线程获取到了, 其他线程尝试acquire就会失败. 共享模式下多个线程的acquire有可能成功. 本类并不"理解"这之间的区别, 即便一个共享模式的acquire成功了, 下一个等待线程也仍然要自行决定它是否能够acquire. 处于不同模式的等待线程共享同一个FIFO队列. 通常来说, 子类实现应该支持其中一种模式, 但也有可能两种都支持, 比如在 ReadWriteLock中. 只支持其中一种模式的子类不用实现另一种模式的方法.  
本类定义了一个内部类, 叫 ConditionObject, 作为Condition的实现, 支持独占模式的子类可以使用它. AbstractQueuedSynchronizer类的其他方法都不能创建这样的condition对象, 否则不应使用这个类. 当然, ConditionObject类的行为依赖于实现它的同步量的语义.  
本类提供对内部队列的检视/插值, 对condition对象也有类似操作. 这些可以暴露给使用AbstractQueuedSynchronizer作为同步机制的类. 序列化本类只会存储代表状态的那个原子整数变量, 反序列化将得不到线程队列. 一般来说子类需要实现自己的readObject来恢复到一个确定的状态.

>Usage
使用本类作为同步量的基础, 需要实现以下方法, 来检视或者更改同步状态变量.  
- tryAcquire
- tryRelease
- tryAcquireShared
- tryReleaseShared
- isHeldExclusively

>每个方法默认都会抛出UnsupportedOperationException. 实现这些方法的时候需要保证内部的线程安全, 一般来说应该简短且非阻塞. 定义这些方法是唯一被支持的使用本类的方式. 所有其他方法都已经被定义为final.  
还有从AbstractOwnableSynchronizer中继承下来有关判断当前持有者线程的方法, 建议使用他们来帮助判断当前线程是否持有同步量.  
尽管内部实现基于FIFO队列, 但本类不自动遵从FIFO的获取策略, 而是通过acquire和release的默认实现调用子类实现来决定获取顺序.  
这是因为获取锁的检查早于入队操作, 新线程尝试获取锁可能会优先于等待队列中阻塞了的线程. 但是你可以通过实现tryAcquire和tryAcquireShared来禁止这种机制, 以达到公平队列的效果.  
默认的barging策略通常来说有更高吞吐量和可扩展性. 虽然它可能引起饥饿, 但是早入队的线程仍然被允许优先于晚入队的线程重新参与竞争, 每次重新竞争都是与到来的线程之间的公平竞争. 而且, 虽然通常acquire过程不自旋, 但是可能调用多次tryAcquire. 这种机制, 在独占同步量被持有的时候保留了自旋机制的大多数好处, 同时在它不被持有的时候又避免了自旋机制的大多数依赖. 如果这是你想要的, 你可以在检查时提前调用acquire.  
本类为依赖于单一整型变量状态的同步量提供一个高效且可扩展的基础实现及一个内部FIFO等待队列. 如果这不够的话, 你可以从更底层的原子类, 自定义的 java.util.Queue类, LockSupport的阻塞支持, 这些东西来构造同步量.  

看完了以上注释, 我们心中有底了:  
这个类提供了一个等待队列, 提供了更改状态的方法, 但是没提供任何跟锁或同步等有关的操作接口.  
这个类可以序列化和反序列化, 但默认只有那个表示状态的整数可以被序列化和反序列化.  

下面来看`AbstractQueuedSynchronizer`类的源码. 需要子类实现的接口我就不列出来了.  
```
// in java/util/concurrent/locks/AbstractQueuedSynchronizer.class
public abstract class AbstractQueuedSynchronizer extends AbstractOwnableSynchronizer implements Serializable {
    private transient volatile AbstractQueuedSynchronizer.Node head; // 队头, 用于实现内部队列
    private transient volatile AbstractQueuedSynchronizer.Node tail; // 队尾, 用于实现内部队列
    private volatile int state; // 状态, 注意它是volatile的
    static final long spinForTimeoutThreshold = 1000L; // 自旋时间阈值
    private static final Unsafe unsafe = Unsafe.getUnsafe(); // 内存操作
    private static final long stateOffset; // 状态偏移
    private static final long headOffset; // 队头偏移
    private static final long tailOffset; // 队尾偏移
    private static final long waitStatusOffset; // 等待状态偏移
    private static final long nextOffset; // 下个节点偏移
    
    // 以上是全部成员变量, 可以看到除了state以外其他的变量要么是static的要么是transient的, 意味着都不能序列化
    
    
    
}
```


## 扩展: 线程安全与非线程安全
常常会在talk和conference中听到线程安全和非线程安全这种词, 本节简单讲讲到底什么是线程安全什么是非线程安全, 并重点过一遍Java中那些常用数据结构哪些是线程安全哪些是非线程安全, 以及为什么.  

## 扩展: Java中的同步关键字synchronized
同步关键字synchonized实现的是可重入锁.  

[AQS源码]:  http://www.grepcode.com/file/repository.grepcode.com/java/root/jdk/openjdk/8u40-b25/java/util/concurrent/locks/AbstractQueuedSynchronizer.java#AbstractQueuedSynchronizer  
[Java并发编程实战-----AQS(一)：简介]: http://www.cnblogs.com/chenssy/p/5054822.html  
[Java AQS AbstractQueuedSynchronizer]: http://www.jianshu.com/p/3ebc18cf7250  
[Java并发机制及锁的实现原理]: http://blog.csdn.net/mine_song/article/details/68946868  
[Java锁----Lock实现原理]: http://blog.csdn.net/endlu/article/details/51249156  
[深入JVM锁机制1-synchronized]: http://blog.csdn.net/chen77716/article/details/6618779  
[Java 8 Concurrency Tutorial: Synchronization and Locks]: http://winterbe.com/posts/2015/04/30/java8-concurrency-tutorial-synchronized-locks-examples/  
[探索 ConcurrentHashMap 高并发性的实现机制]: https://www.ibm.com/developerworks/cn/java/java-lo-concurrenthashmap/index.html  

